import os
import utils.utils_saving_result as utils_saving_result

import tensorflow as tf
import tensorflow_addons as tfa
import numpy as np

from maml import ModelAgnosticMetaLearningModel
from utils.networks import SimpleModel, MiniImagenetModel, CRCModel
from utils.tf_datasets import OmniglotDatabase, MiniImagenetDatabase, CRCDatabase, CRCDatabase_cropped

# https://stackoverflow.com/questions/35911252/disable-tensorflow-debugging-information
tf.get_logger().setLevel('ERROR')

class UMTRA(ModelAgnosticMetaLearningModel):
    def __init__(
            self,
            database,
            network_cls,
            n,
            meta_batch_size,
            num_steps_ml,
            lr_inner_ml,
            num_steps_validation,
            save_after_epochs,
            meta_learning_rate,
            report_validation_frequency,
            log_train_images_after_iteration,  # Set to -1 if you do not want to log train images.
            least_number_of_tasks_val_test=-1,  # Make sure the val and test dataset pick at least this many tasks.
            clip_gradients=False,
            augmentation_function=None,
            k_shot=1,    #--> number of shots in test and validation phases. Note that in UMTRA, training is always 1-shot (2-shot if considering augmentation, too)
            dataset_name="",
            original_UMTRA=True
    ):
        self.augmentation_function = augmentation_function
        self.original_UMTRA = original_UMTRA
        super(UMTRA, self).__init__(
            database=database,
            network_cls=network_cls,
            n=n,
            k=k_shot,
            meta_batch_size=meta_batch_size,
            num_steps_ml=num_steps_ml,
            lr_inner_ml=lr_inner_ml,
            num_steps_validation=num_steps_validation,
            save_after_epochs=save_after_epochs,
            meta_learning_rate=meta_learning_rate,
            report_validation_frequency=report_validation_frequency,
            log_train_images_after_iteration=log_train_images_after_iteration,
            least_number_of_tasks_val_test=least_number_of_tasks_val_test,
            clip_gradients=clip_gradients,
            dataset_name=dataset_name
        )

    def get_root(self):
        return os.path.dirname(__file__)

    # def get_train_dataset(self):
    #     dataset = self.database.get_umtra_dataset(
    #         self.database.train_folders,
    #         n=self.n,
    #         meta_batch_size=self.meta_batch_size,
    #         augmentation_function=self.augmentation_function
    #     )
    #     return dataset

    def get_train_dataset(self):
        if self.original_UMTRA:  #--> original UMTRA which always has 1-shot in training and any few-shot in testing
            dataset = self.database.get_umtra_dataset(
                self.database.train_folders,
                n=self.n,
                meta_batch_size=self.meta_batch_size,
                augmentation_function=self.augmentation_function
            )
        else:   #--> modified UMTRA which has any few-shot in both training and testing
            dataset = self.database.get_umtra_dataset_multiple_augmentation(
                self.database.train_folders,
                n=self.n,
                meta_batch_size=self.meta_batch_size,
                augmentation_function=self.augmentation_function,
                n_augmentation=(self.k*2)-1
            )
        return dataset
        
    def get_config_info(self):
        return f'umtra_' \
               f'model-{self.network_cls.name}_' \
               f'mbs-{self.meta_batch_size}_' \
               f'n-{self.n}_' \
               f'k-{self.k}_' \
               f'stp-{self.num_steps_ml}'


def run_omniglot(n_way=5, k_shot=1, do_train=True, epoch_to_load_for_test=1, epoch_to_load_for_train=None,
                      epochs_training=100, iterations_test=50, meta_batch_size=32, get_embedding_in_test_phase=False, original_UMTRA=True):
    @tf.function
    def augment(images):
        result = list()
        num_imgs = 1
        for i in range(images.shape[0]):
            image = tf.reshape(images[i], (num_imgs, 28, 28, 1))
            random_map = tf.random.uniform(shape=tf.shape(image), minval=0, maxval=2, dtype=tf.int32)
            random_map = tf.cast(random_map, tf.float32)
            image = tf.minimum(image, random_map)

            base_ = tf.convert_to_tensor(np.tile([1, 0, 0, 0, 1, 0, 0, 0], [num_imgs, 1]), dtype=tf.float32)
            mask_ = tf.convert_to_tensor(np.tile([0, 0, 1, 0, 0, 1, 0, 0], [num_imgs, 1]), dtype=tf.float32)
            random_shift_ = tf.random.uniform([num_imgs, 8], minval=-6., maxval=6., dtype=tf.float32)
            transforms_ = base_ + random_shift_ * mask_
            augmented_data = tfa.image.transform(images=image, transforms=transforms_)
            result.append(augmented_data)

        return tf.stack(result)

    omniglot_database = OmniglotDatabase(
        random_seed=-1,
        num_train_classes=1200,
        num_val_classes=100,
    )

    umtra = UMTRA(
        database=omniglot_database,
        network_cls=SimpleModel,
        n=n_way,
        meta_batch_size=meta_batch_size,
        num_steps_ml=5,
        lr_inner_ml=0.01,
        num_steps_validation=5,
        save_after_epochs=5,
        meta_learning_rate=0.001,
        log_train_images_after_iteration=10,
        report_validation_frequency=1,
        augmentation_function=augment,
        k_shot=k_shot,    #--> number of shots in test and validation phases. Note that in UMTRA, training is always 1-shot (2-shot if considering augmentation, too)
        dataset_name="omniglot",
        original_UMTRA=original_UMTRA
    )

    if do_train:
        umtra.train(epochs=epochs_training, epochs_to_load_from=epoch_to_load_for_train)
    else:
        umtra.evaluate(iterations=iterations_test, epochs_to_load_from=epoch_to_load_for_test, get_embedding=get_embedding_in_test_phase, network_has_class_layer=True)

def run_mini_imagenet(n_way=5, k_shot=1, do_train=True, epoch_to_load_for_test=1, epoch_to_load_for_train=None,
                      epochs_training=100, iterations_test=50, meta_batch_size=16, get_embedding_in_test_phase=False, original_UMTRA=True):
    mini_imagenet_database = MiniImagenetDatabase(random_seed=-1)

    @tf.function
    def augment(images):
        images = tf.squeeze(images, axis=1)

        if tf.random.uniform(shape=(), minval=0, maxval=1) > 0.5:
            images = tf.image.rgb_to_grayscale(images)
            images = tf.squeeze(images, axis=-1)
            images = tf.stack((images, images, images), axis=-1)
        else:
            images = tf.image.random_brightness(images, max_delta=0.4)
            images = tf.image.random_hue(images, max_delta=0.4)

        if tf.random.uniform(shape=(), minval=0, maxval=1) > 0.7:
            random_map = tf.random.uniform(shape=tf.shape(images)[:-1], minval=0, maxval=2, dtype=tf.int32)
            random_map = tf.stack((random_map, random_map, random_map), axis=-1)
            random_map = tf.cast(random_map, tf.float32)
            images = tf.minimum(images, random_map)

        if tf.random.uniform(shape=(), minval=0, maxval=1) > 0.5:
            transforms = [
                1,
                0,
                -tf.random.uniform(shape=(), minval=-40, maxval=40, dtype=tf.int32),
                0,
                1,
                -tf.random.uniform(shape=(), minval=-40, maxval=40, dtype=tf.int32),
                0,
                0
            ]
            images = tfa.image.transform(images, transforms)

        if tf.random.uniform(shape=(), minval=0, maxval=1) > 0.7:
            images = tfa.image.rotate(images, tf.random.uniform(shape=(5, ), minval=-30, maxval=30))

        if tf.random.uniform(shape=(), minval=0, maxval=1) > 0.7:
            images = tf.image.random_crop(images, size=(5, 42, 42, 3))
            images = tf.image.resize(images, size=(84, 84))

        return tf.reshape(images, (5, 1, 84, 84, 3))

    if do_train:
        umtra = UMTRA(
            database=mini_imagenet_database,
            network_cls=MiniImagenetModel,
            n=n_way,
            meta_batch_size=meta_batch_size,
            num_steps_ml=1,
            lr_inner_ml=0.05,
            num_steps_validation=1,
            save_after_epochs=1,
            meta_learning_rate=0.01,
            report_validation_frequency=1,
            log_train_images_after_iteration=10,
            least_number_of_tasks_val_test=100,
            clip_gradients=True,
            augmentation_function=augment,
            k_shot=k_shot,    #--> number of shots in test and validation phases. Note that in UMTRA, training is always 1-shot (2-shot if considering augmentation, too)
            dataset_name="mini_imagenet",
            original_UMTRA=original_UMTRA
        )
        umtra.train(epochs=epochs_training, epochs_to_load_from=epoch_to_load_for_train)
    else:
        umtra = UMTRA(
            database=mini_imagenet_database,
            network_cls=MiniImagenetModel,
            n=n_way,
            meta_batch_size=meta_batch_size,
            num_steps_ml=1,
            lr_inner_ml=0.05,
            num_steps_validation=1,
            save_after_epochs=1,
            meta_learning_rate=0.001,
            report_validation_frequency=1,
            log_train_images_after_iteration=10,
            least_number_of_tasks_val_test=100,
            clip_gradients=True,
            augmentation_function=augment,
            k_shot=k_shot,    #--> number of shots in test and validation phases. Note that in UMTRA, training is always 1-shot (2-shot if considering augmentation, too)
            dataset_name="mini_imagenet",
            original_UMTRA=original_UMTRA
        )
        umtra.evaluate(iterations=iterations_test, epochs_to_load_from=epoch_to_load_for_test, get_embedding=get_embedding_in_test_phase, network_has_class_layer=True)

def run_CRC(n_way=5, k_shot=1, do_train=True, epoch_to_load_for_test=1, epoch_to_load_for_train=None, 
                      epochs_training=100, iterations_test=50, meta_batch_size=16, get_embedding_in_test_phase=False, original_UMTRA=True):
    CRC_database = CRCDatabase(random_seed=-1)

    IMG_HEIGHT, IMG_WIDTH = 224, 224

    @tf.function
    def augment(images):
        images = tf.squeeze(images, axis=1)

        if tf.random.uniform(shape=(), minval=0, maxval=1) > 0.5:
            images = tf.image.rgb_to_grayscale(images)
            images = tf.squeeze(images, axis=-1)
            images = tf.stack((images, images, images), axis=-1)
        else:
            images = tf.image.random_brightness(images, max_delta=0.4)
            images = tf.image.random_hue(images, max_delta=0.4)

        if tf.random.uniform(shape=(), minval=0, maxval=1) > 0.7:
            random_map = tf.random.uniform(shape=tf.shape(images)[:-1], minval=0, maxval=2, dtype=tf.int32)
            random_map = tf.stack((random_map, random_map, random_map), axis=-1)
            random_map = tf.cast(random_map, tf.float32)
            images = tf.minimum(images, random_map)

        if tf.random.uniform(shape=(), minval=0, maxval=1) > 0.5:
            transforms = [
                1,
                0,
                -tf.random.uniform(shape=(), minval=-40, maxval=40, dtype=tf.int32),
                0,
                1,
                -tf.random.uniform(shape=(), minval=-40, maxval=40, dtype=tf.int32),
                0,
                0
            ]
            images = tfa.image.transform(images, transforms)

        if tf.random.uniform(shape=(), minval=0, maxval=1) > 0.7:
            images = tfa.image.rotate(images, tf.random.uniform(shape=(5, ), minval=-30, maxval=30))

        #--> in pathology, we should not resize image, so we comment this
        # if tf.random.uniform(shape=(), minval=0, maxval=1) > 0.7:
        #     images = tf.image.random_crop(images, size=(5, IMG_HEIGHT//2, IMG_WIDTH//2, 3))
        #     images = tf.image.resize(images, size=(IMG_HEIGHT, IMG_WIDTH))

        return tf.reshape(images, (5, 1, IMG_HEIGHT, IMG_WIDTH, 3))

    if do_train:
        umtra = UMTRA(
            database=CRC_database,
            network_cls=CRCModel,
            n=n_way,
            meta_batch_size=meta_batch_size,
            num_steps_ml=1,
            lr_inner_ml=0.005, 
            num_steps_validation=1,
            save_after_epochs=1,
            meta_learning_rate=0.01,
            report_validation_frequency=1,
            log_train_images_after_iteration=10,
            least_number_of_tasks_val_test=100,
            clip_gradients=True,
            augmentation_function=augment,
            k_shot=k_shot,
            dataset_name="CRC",
            original_UMTRA=original_UMTRA
        )
        umtra.train(epochs=epochs_training, epochs_to_load_from=epoch_to_load_for_train)
    else:
        umtra = UMTRA(
            database=CRC_database,
            network_cls=CRCModel,
            n=n_way,
            meta_batch_size=meta_batch_size,
            num_steps_ml=1,
            lr_inner_ml=0.01,
            num_steps_validation=1,
            save_after_epochs=1,
            meta_learning_rate=0.001,
            report_validation_frequency=1,
            log_train_images_after_iteration=10,
            least_number_of_tasks_val_test=100,
            clip_gradients=True,
            augmentation_function=augment,
            k_shot=k_shot,
            dataset_name="CRC",
            original_UMTRA=original_UMTRA
        )
        umtra.evaluate(iterations=iterations_test, epochs_to_load_from=epoch_to_load_for_test, get_embedding=get_embedding_in_test_phase, network_has_class_layer=True)

def run_CRC_cropped(n_way=5, k_shot=1, do_train=True, epoch_to_load_for_test=1, epoch_to_load_for_train=None,
                      epochs_training=100, iterations_test=50, meta_batch_size=16, get_embedding_in_test_phase=True, original_UMTRA=True):
    crcDatabase_cropped = CRCDatabase_cropped(random_seed=-1)

    @tf.function
    def augment(images):
        images = tf.squeeze(images, axis=1)

        if tf.random.uniform(shape=(), minval=0, maxval=1) > 0.5:
            images = tf.image.rgb_to_grayscale(images)
            images = tf.squeeze(images, axis=-1)
            images = tf.stack((images, images, images), axis=-1)
        else:
            images = tf.image.random_brightness(images, max_delta=0.4)
            images = tf.image.random_hue(images, max_delta=0.4)

        if tf.random.uniform(shape=(), minval=0, maxval=1) > 0.7:
            random_map = tf.random.uniform(shape=tf.shape(images)[:-1], minval=0, maxval=2, dtype=tf.int32)
            random_map = tf.stack((random_map, random_map, random_map), axis=-1)
            random_map = tf.cast(random_map, tf.float32)
            images = tf.minimum(images, random_map)

        if tf.random.uniform(shape=(), minval=0, maxval=1) > 0.5:
            transforms = [
                1,
                0,
                -tf.random.uniform(shape=(), minval=-40, maxval=40, dtype=tf.int32),
                0,
                1,
                -tf.random.uniform(shape=(), minval=-40, maxval=40, dtype=tf.int32),
                0,
                0
            ]
            images = tfa.image.transform(images, transforms)

        if tf.random.uniform(shape=(), minval=0, maxval=1) > 0.7:
            images = tfa.image.rotate(images, tf.random.uniform(shape=(5, ), minval=-30, maxval=30))

        #--> in pathology, we should not resize image, so we comment this
        # if tf.random.uniform(shape=(), minval=0, maxval=1) > 0.7:
        #     images = tf.image.random_crop(images, size=(5, 42, 42, 3))
        #     images = tf.image.resize(images, size=(84, 84))

        return tf.reshape(images, (5, 1, 84, 84, 3))

    if do_train:
        umtra = UMTRA(
            database=crcDatabase_cropped,
            network_cls=MiniImagenetModel,
            n=n_way,
            meta_batch_size=meta_batch_size,
            num_steps_ml=1,
            lr_inner_ml=0.05,
            num_steps_validation=1,
            save_after_epochs=1,
            meta_learning_rate=0.01,
            report_validation_frequency=1,
            log_train_images_after_iteration=10,
            least_number_of_tasks_val_test=100,
            clip_gradients=True,
            augmentation_function=augment,
            k_shot=k_shot,    #--> number of shots in test and validation phases. Note that in UMTRA, training is always 1-shot (2-shot if considering augmentation, too)
            dataset_name="CRC_cropped",
            original_UMTRA=original_UMTRA
        )
        umtra.train(epochs=epochs_training, epochs_to_load_from=epoch_to_load_for_train)
    else:
        umtra = UMTRA(
            database=crcDatabase_cropped,
            network_cls=MiniImagenetModel,
            n=n_way,
            meta_batch_size=meta_batch_size,
            num_steps_ml=1,
            lr_inner_ml=0.05,
            num_steps_validation=1,
            save_after_epochs=1,
            meta_learning_rate=0.001,
            report_validation_frequency=1,
            log_train_images_after_iteration=10,
            least_number_of_tasks_val_test=100,
            clip_gradients=True,
            augmentation_function=augment,
            k_shot=k_shot,    #--> number of shots in test and validation phases. Note that in UMTRA, training is always 1-shot (2-shot if considering augmentation, too)
            dataset_name="CRC_cropped",
            original_UMTRA=original_UMTRA
        )
        umtra.evaluate(iterations=iterations_test, epochs_to_load_from=epoch_to_load_for_test, get_embedding=get_embedding_in_test_phase, network_has_class_layer=True)


if __name__ == '__main__':
    # settings:
    do_train = True  #--> True: train, False: test
    dataset = "mini_imagenet"  #--> omniglot, mini_imagenet, CRC, CRC_cropped
    original_UMTRA = True  #--> True: original UMTRA which always has 1-shot in training and any few-shot in testing || False: modified UMTRA which has any few-shot in both training and testing
    n_way = 5
    k_shot = 1  #--> number of shots in test and validation phases. Note that in UMTRA, training is always 1-shot (2-shot if considering augmentation, too)
    epoch_to_load_for_test = 1
    epoch_to_load_for_train = None  #--> if None, it trains from scratch
    epochs_training = 40  #--> 40 for mini-imagenet, 10 for omniglot
    iterations_test = 50  #--> 50 for mini-imagenet, 50 for omniglot
    meta_batch_size = 16  #--> 16 for mini-imagenet, 32 for omniglot
    get_embedding_in_test_phase = True

    if dataset == "omniglot":
        run_omniglot(n_way, k_shot, do_train, epoch_to_load_for_test, epoch_to_load_for_train, epochs_training, iterations_test, meta_batch_size, get_embedding_in_test_phase, original_UMTRA)
    elif dataset == "mini_imagenet":
        run_mini_imagenet(n_way, k_shot, do_train, epoch_to_load_for_test, epoch_to_load_for_train, epochs_training, iterations_test, meta_batch_size, get_embedding_in_test_phase, original_UMTRA)
    elif dataset == "CRC":
        run_CRC(n_way, k_shot, do_train, epoch_to_load_for_test, epoch_to_load_for_train, epochs_training, iterations_test, meta_batch_size, get_embedding_in_test_phase, original_UMTRA)
    elif dataset == "CRC_cropped":
        run_CRC_cropped(n_way, k_shot, do_train, epoch_to_load_for_test, epoch_to_load_for_train, epochs_training, iterations_test, meta_batch_size, get_embedding_in_test_phase, original_UMTRA)
