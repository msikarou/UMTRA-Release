import os
import utils.utils_saving_result as utils_saving_result

import tensorflow as tf
import numpy as np
from tqdm import tqdm

from utils.tf_datasets import OmniglotDatabase, MiniImagenetDatabase, CRCDatabase, CRCDatabase_cropped
from utils.networks import SimpleModel, MiniImagenetModel, CRCModel
from utils.base_model import  BaseModel
from utils.utils import combine_first_two_axes, average_gradients
import utils.settings as settings

DEBUGGING = False
DEBUGGING_PLOT_INPUT = False

class ModelAgnosticMetaLearningModel(BaseModel):
    def __init__(
        self,
        database,
        network_cls,
        n,
        k,
        meta_batch_size,
        num_steps_ml,
        lr_inner_ml,
        num_steps_validation,
        save_after_epochs,
        meta_learning_rate,
        report_validation_frequency,
        log_train_images_after_iteration,  # Set to -1 if you do not want to log train images.
        least_number_of_tasks_val_test=-1,  # Make sure the validaiton and test dataset pick at least this many tasks.
        clip_gradients=False,
        dataset_name=""
    ):
        self.n = n
        self.k = k
        self.meta_batch_size = meta_batch_size
        self.num_steps_ml = num_steps_ml
        self.lr_inner_ml = lr_inner_ml
        self.num_steps_validation = num_steps_validation
        self.save_after_epochs = save_after_epochs
        self.log_train_images_after_iteration = log_train_images_after_iteration
        self.report_validation_frequency = report_validation_frequency
        self.least_number_of_tasks_val_test = least_number_of_tasks_val_test
        self.clip_gradients = clip_gradients
        super(ModelAgnosticMetaLearningModel, self).__init__(database, network_cls)

        self.model = self.network_cls(num_classes=self.n)
        self.model(tf.zeros(shape=(n * k, *self.database.input_shape)))

        self.updated_models = list()
        for _ in range(self.num_steps_ml + 1):
            updated_model = self.network_cls(num_classes=self.n)
            updated_model(tf.zeros(shape=(n * k, *self.database.input_shape)))
            self.updated_models.append(updated_model)

        self.optimizer = tf.keras.optimizers.Adam(learning_rate=meta_learning_rate)
        self.val_accuracy_metric = tf.metrics.Accuracy()
        self.val_loss_metric = tf.metrics.Mean()

        self._root = self.get_root()
        self.train_log_dir = os.path.join(self._root, self.get_config_info(), 'logs/train/')
        self.train_summary_writer = tf.summary.create_file_writer(self.train_log_dir)
        self.val_log_dir = os.path.join(self._root, self.get_config_info(), 'logs/val/')
        self.val_summary_writer = tf.summary.create_file_writer(self.val_log_dir)

        # self.checkpoint_dir = os.path.join(self._root, self.get_config_info(), 'saved_models/')
        self.checkpoint_dir = os.path.join(self._root, 'saved_files\\saved_models\\')

        self.train_accuracy_metric = tf.metrics.Accuracy()
        self.train_loss_metric = tf.metrics.Mean()

        if dataset_name == "":
            raise ValueError("A valid dataset name should be given")
        self.dataset_name = dataset_name

    def get_root(self):
        # return os.path.dirname(__file__)
        return ".\\saved_files\\"

    def get_train_dataset(self):
        dataset = self.database.get_supervised_meta_learning_dataset(
            self.database.train_folders,
            n=self.n,
            k=self.k,
            meta_batch_size=self.meta_batch_size
        )
        # steps_per_epoch = dataset.steps_per_epoch
        # dataset = dataset.prefetch(1)
        # setattr(dataset, 'steps_per_epoch', steps_per_epoch)
        return dataset

    def get_val_dataset(self):
        val_dataset = self.database.get_supervised_meta_learning_dataset(
            self.database.val_folders,
            n=self.n,
            k=self.k,
            meta_batch_size=1
        )
        steps_per_epoch = max(val_dataset.steps_per_epoch, self.least_number_of_tasks_val_test)
        val_dataset = val_dataset.repeat(-1)
        val_dataset = val_dataset.take(steps_per_epoch)
        setattr(val_dataset, 'steps_per_epoch', steps_per_epoch)
        return val_dataset

    def get_val_dataset_withRealLabels(self):
        # we have changed the following function to also get the real labels in addition to the data and generated labels for meta-learning
        # have changed "get_supervised_meta_learning" to "get_supervised_meta_learning_dataset_WithRealLabels"
        # look at these functions in file tf_datasets.py
        val_dataset = self.database.get_supervised_meta_learning_dataset_WithRealLabels(
            self.database.val_folders,
            n=self.n,
            k=1,
            meta_batch_size=1,
        )
        steps_per_epoch = max(val_dataset.steps_per_epoch, self.least_number_of_tasks_val_test)
        val_dataset = val_dataset.repeat(-1)
        val_dataset = val_dataset.take(steps_per_epoch)
        setattr(val_dataset, 'steps_per_epoch', steps_per_epoch)
        return val_dataset

    def get_test_dataset(self):
        test_dataset = self.database.get_supervised_meta_learning_dataset(
            self.database.test_folders,
            n=self.n,
            k=self.k,
            meta_batch_size=1,
        )
        steps_per_epoch = max(test_dataset.steps_per_epoch, self.least_number_of_tasks_val_test)
        test_dataset = test_dataset.repeat(-1)
        test_dataset = test_dataset.take(steps_per_epoch)
        setattr(test_dataset, 'steps_per_epoch', steps_per_epoch)
        return test_dataset

    def get_test_dataset_withRealLabels(self):
        # we have changed the following function to also get the real labels in addition to the data and generated labels for meta-learning
        # have changed "get_supervised_meta_learning" to "get_supervised_meta_learning_dataset_WithRealLabels"
        # look at these functions in file tf_datasets.py
        test_dataset = self.database.get_supervised_meta_learning_dataset_WithRealLabels(
            self.database.test_folders,
            n=self.n,
            k=1,
            meta_batch_size=1,
        )
        steps_per_epoch = max(test_dataset.steps_per_epoch, self.least_number_of_tasks_val_test)
        test_dataset = test_dataset.repeat(-1)
        test_dataset = test_dataset.take(steps_per_epoch)
        setattr(test_dataset, 'steps_per_epoch', steps_per_epoch)
        return test_dataset

    def get_config_info(self):
        return f'model-{self.network_cls.name}_' \
               f'mbs-{self.meta_batch_size}_' \
               f'n-{self.n}_' \
               f'k-{self.k}_' \
               f'stp-{self.num_steps_ml}'

    def create_meta_model(self, updated_model, model, gradients):
        k = 0
        variables = list()

        for i in range(len(model.layers)):
            if isinstance(model.layers[i], tf.keras.layers.Conv2D) or \
             isinstance(model.layers[i], tf.keras.layers.Dense):
                updated_model.layers[i].kernel = model.layers[i].kernel - self.lr_inner_ml * gradients[k]
                k += 1
                variables.append(updated_model.layers[i].kernel)

                updated_model.layers[i].bias = model.layers[i].bias - self.lr_inner_ml * gradients[k]
                k += 1
                variables.append(updated_model.layers[i].bias)

            elif isinstance(model.layers[i], tf.keras.layers.BatchNormalization):
                if hasattr(model.layers[i], 'moving_mean') and model.layers[i].moving_mean is not None:
                    updated_model.layers[i].moving_mean.assign(model.layers[i].moving_mean)
                if hasattr(model.layers[i], 'moving_variance') and model.layers[i].moving_variance is not None:
                    updated_model.layers[i].moving_variance.assign(model.layers[i].moving_variance)
                if hasattr(model.layers[i], 'gamma') and model.layers[i].gamma is not None:
                    updated_model.layers[i].gamma = model.layers[i].gamma - self.lr_inner_ml * gradients[k]
                    k += 1
                    variables.append(updated_model.layers[i].gamma)
                if hasattr(model.layers[i], 'beta') and model.layers[i].beta is not None:
                    updated_model.layers[i].beta = \
                        model.layers[i].beta - self.lr_inner_ml * gradients[k]
                    k += 1
                    variables.append(updated_model.layers[i].beta)

            elif isinstance(model.layers[i], tf.keras.layers.LayerNormalization):
                if hasattr(model.layers[i], 'gamma') and model.layers[i].gamma is not None:
                    updated_model.layers[i].gamma = model.layers[i].gamma - self.lr_inner_ml * gradients[k]
                    k += 1
                    variables.append(updated_model.layers[i].gamma)
                if hasattr(model.layers[i], 'beta') and model.layers[i].beta is not None:
                    updated_model.layers[i].beta = \
                        model.layers[i].beta - self.lr_inner_ml * gradients[k]
                    k += 1
                    variables.append(updated_model.layers[i].beta)


        setattr(updated_model, 'meta_trainable_variables', variables)

    def get_train_loss_and_gradients(self, train_ds, train_labels):
        with tf.GradientTape(persistent=True) as train_tape:
            # TODO compare between model.forward(train_ds) and model(train_ds)
            logits = self.model(train_ds, training=True)
            train_loss = tf.reduce_sum(tf.losses.categorical_crossentropy(train_labels, logits, from_logits=True))

        train_gradients = train_tape.gradient(train_loss, self.model.trainable_variables)
        return train_loss, train_gradients, train_tape

    # def get_task_train_and_val_ds(self, task, labels):
    #     train_ds, val_ds = tf.split(task, num_or_size_splits=2)
    #     train_labels, val_labels = tf.split(labels, num_or_size_splits=2)
        
    #     train_ds = combine_first_two_axes(tf.squeeze(train_ds, axis=0))
    #     val_ds = combine_first_two_axes(tf.squeeze(val_ds, axis=0))
    #     train_labels = combine_first_two_axes(tf.squeeze(train_labels, axis=0))
    #     val_labels = combine_first_two_axes(tf.squeeze(val_labels, axis=0))

    #     return train_ds, val_ds, train_labels, val_labels

    def get_task_train_and_val_ds(self, task, labels):
        train_ds, val_ds = tf.split(task, num_or_size_splits=2)
        train_labels, val_labels = tf.split(labels, num_or_size_splits=2)
        
        if train_ds.shape[0] == 1:
            axis_squeez = 0
        elif train_ds.shape[2] == 1:
            axis_squeez = 2
        train_ds = combine_first_two_axes(tf.squeeze(train_ds, axis=axis_squeez))
        val_ds = combine_first_two_axes(tf.squeeze(val_ds, axis=axis_squeez))
        train_labels = combine_first_two_axes(tf.squeeze(train_labels, axis=axis_squeez))
        val_labels = combine_first_two_axes(tf.squeeze(val_labels, axis=axis_squeez))

        # for debugging and plotting (put breakpoint here and run these codes in debug consule):
        if DEBUGGING_PLOT_INPUT:
            import matplotlib.pyplot as plt
            print(train_ds.shape)  #--> (n_augmentation * n_way) * 84 * 83 * 3
            print(train_labels)  #--> shape: (n_augmentation * n_way) * n_way --> second n_way is because of one-hot encoding
            plt.imshow(train_ds[0,:,:,:])
            plt.show()
            print(val_ds.shape)  #--> (n_augmentation * n_way) * 84 * 83 * 3
            print(val_labels)  #--> shape: (n_augmentation * n_way) * n_way --> second n_way is because of one-hot encoding
            plt.imshow(val_ds[0,:,:,:])
            plt.show()

        return train_ds, val_ds, train_labels, val_labels

    def inner_train_loop(self, train_ds, train_labels, num_iterations=-1):
        if num_iterations == -1:
            num_iterations = self.num_steps_ml

            gradients = list()
            for variable in self.model.trainable_variables:
                gradients.append(tf.zeros_like(variable))

            self.create_meta_model(self.updated_models[0], self.model, gradients)

            for k in range(1, num_iterations + 1):
                with tf.GradientTape(persistent=True) as train_tape:
                    train_tape.watch(self.updated_models[k - 1].meta_trainable_variables)
                    logits = self.updated_models[k - 1](train_ds, training=True)
                    loss = tf.reduce_sum(
                        tf.losses.categorical_crossentropy(train_labels, logits, from_logits=True)
                    )
                gradients = train_tape.gradient(loss, self.updated_models[k - 1].meta_trainable_variables)
                self.create_meta_model(self.updated_models[k], self.updated_models[k - 1], gradients)

            return self.updated_models[-1]

        else:
            gradients = list()
            for variable in self.model.trainable_variables:
                gradients.append(tf.zeros_like(variable))

            self.create_meta_model(self.updated_models[0], self.model, gradients)
            copy_model = self.updated_models[0]

            for k in range(num_iterations):
                with tf.GradientTape(persistent=True) as train_tape:
                    train_tape.watch(copy_model.meta_trainable_variables)
                    logits = copy_model(train_ds, training=True)
                    loss = tf.reduce_sum(
                        tf.losses.categorical_crossentropy(train_labels, logits, from_logits=True)
                    )
                gradients = train_tape.gradient(loss, copy_model.meta_trainable_variables)
                self.create_meta_model(copy_model, copy_model, gradients)

            return copy_model

    def save_model(self, epochs):
        # self.model.save_weights(os.path.join(self.checkpoint_dir, f'model.ckpt-{epochs}'))
        # commented this line for this error --> https://stackoverflow.com/questions/45076911/tensorflow-failed-to-create-a-newwriteablefile-when-retraining-inception
        path_ = ".\\saved_files\\saved_models\\epoch{}\\".format(epochs) + "model.ckpt-{}".format(epochs)
        self.model.save_weights(path_)

    def load_model(self, epochs=None):
        epoch_count = 0
        if epochs is not None:
            checkpoint_path = os.path.join(self.checkpoint_dir + "epoch{}\\".format(epochs), f'model.ckpt-{epochs}')
            epoch_count = epochs
        else:
            checkpoint_path = tf.train.latest_checkpoint(self.checkpoint_dir + "epoch{}\\".format(epochs))

        if checkpoint_path is not None:
            try:
                self.model.load_weights(checkpoint_path)
                print('==================\nResuming Training\n==================')
                epoch_count = int(checkpoint_path[checkpoint_path.rindex('-') + 1:])
            except Exception as e:
                print('Could not load the previous checkpoint!')

        else:
            print('No previous checkpoint found!')

        return epoch_count

    def log_images(self, summary_writer, train_ds, val_ds, step):
        with tf.device('cpu:0'):
            with summary_writer.as_default():
                tf.summary.image(
                    'train',
                    train_ds,
                    step=step,
                    max_outputs=5
                )
                tf.summary.image(
                    'validation',
                    val_ds,
                    step=step,
                    max_outputs=5
                )

    def update_loss_and_accuracy(self, logits, labels, loss_metric, accuracy_metric, embeddings=None):
        # embeddings is not used in MAML and UMTRA but is used in the children classes MAML_lossEmbedding and UMTRA_lossEmbedding
        # print(tf.argmax(logits, axis=-1))
        val_loss = tf.reduce_sum(
            tf.losses.categorical_crossentropy(labels, logits, from_logits=True))
        loss_metric.update_state(val_loss)
        accuracy_metric.update_state(
            tf.argmax(labels, axis=-1),
            tf.argmax(logits, axis=-1)
        )

    def log_metric(self, summary_writer, name, metric, step):
        with summary_writer.as_default():
            tf.summary.scalar(name, metric.result(), step=step)

    def evaluate(self, iterations, epochs_to_load_from=None, get_embedding=True, network_has_class_layer=True):
        if self.k == 1:
            self.test_dataset_withRealLabels = self.get_test_dataset_withRealLabels()
        else:
            self.test_dataset = self.get_test_dataset()
            self.test_dataset_withRealLabels = self.get_test_dataset_withRealLabels()
        self.load_model(epochs=epochs_to_load_from)
        # test_log_dir = os.path.join(self._root, self.get_config_info(), 'logs/test/')
        # test_summary_writer = tf.summary.create_file_writer(test_log_dir)

        if self.k == 1:
            self.__evaluate_test_for_1Shot(iterations, get_embedding=get_embedding, network_has_class_layer=network_has_class_layer)
        else:
            self.__evaluate_test_for_fewShot_OnlyLossAndAccuracy(iterations, network_has_class_layer=network_has_class_layer)
            if get_embedding:
                self.__evaluate_test_for_fewShot_OnlyEmbeddingAnalysis(iterations, network_has_class_layer=network_has_class_layer)

    def __evaluate_test_for_1Shot(self, iterations, get_embedding=True, network_has_class_layer=True):
        test_accuracy_metric = tf.metrics.Accuracy()
        test_loss_metric = tf.metrics.Mean()

        first_embedding = True
        embeddings = None
        labels_of_embeddings = []
        itr = 0
        for tmb, lmb, real_labels in self.test_dataset_withRealLabels:
            print("test loading number: {}, number of test samples processed: {} ...".format(itr, itr*2*self.n)) #--> 2 is because we have 2 samples per class in each batch
            itr += 1
            if DEBUGGING and itr >= 5:  #--> for debugging
                break
            for task, labels, real_label in zip(tmb, lmb, real_labels):
                train_ds, val_ds, train_labels, val_labels = self.get_task_train_and_val_ds(task, labels)
                updated_model = self.inner_train_loop(train_ds, train_labels, iterations)
                # If you want to compare with MAML paper, please set the training=True in the following line
                # In that paper the assumption is that we have access to all of test data together and we can evaluate
                # mean and variance from the batch which is given. Make sure to do the same thing in validation.
                updated_model_logits = updated_model(val_ds, training=True)
                
                # get the embeddings:
                if get_embedding:
                    if network_has_class_layer:
                        embedding1 = updated_model.get_embedding(train_ds, training=True)
                        embedding2 = updated_model.get_embedding(val_ds, training=True)
                    else:
                        embedding1 = updated_model(train_ds, training=True)
                        embedding2 = updated_model_logits
                    embedding2_ = embedding2
                    embedding1 = embedding1.numpy()
                    embedding2 = embedding2.numpy()
                    # Note: train_labels are the fake labels for n-way in meta-learning
                    # extract real label from the image path:
                    embedding1_labels = real_label[0,:,:].numpy()
                    embedding1_labels = embedding1_labels.ravel()
                    embedding1_labels = [str(embedding1_labels[i]).split("\\")[-1].split("/")[0] for i in range(len(embedding1_labels))]
                    embedding2_labels = embedding1_labels   #--> The labels of the second set in pairs is equal to the labels of the first set of points in the pairs.
                    if first_embedding:
                        first_embedding = False
                        embeddings = embedding1
                    else:
                        embeddings = np.vstack((embeddings, embedding1))
                    embeddings = np.vstack((embeddings, embedding2))
                    labels_of_embeddings.extend(embedding1_labels)
                    labels_of_embeddings.extend(embedding2_labels)

                self.update_loss_and_accuracy(updated_model_logits, val_labels, test_loss_metric, test_accuracy_metric, embedding2_)

            # self.log_metric(test_summary_writer, 'Loss', test_loss_metric, step=1)
            # self.log_metric(test_summary_writer, 'Accuracy', test_accuracy_metric, step=1)

            print('Test Loss: {}'.format(test_loss_metric.result().numpy()))
            print('Test Accuracy: {}'.format(test_accuracy_metric.result().numpy()))

        path_to_save = "./saved_files/test_results/"
        utils_saving_result.append_to_text_file(path_to_save, name_of_file="test_loss", line_to_add="{} \n".format(test_loss_metric.result().numpy()))
        utils_saving_result.append_to_text_file(path_to_save, name_of_file="test_accuracy", line_to_add="{} \n".format(test_accuracy_metric.result().numpy()))

        if get_embedding:
            utils_saving_result.save_variable(variable=embeddings, name_of_variable="embeddings", path_to_save=path_to_save)
            utils_saving_result.save_variable(variable=labels_of_embeddings, name_of_variable="labels_of_embeddings", path_to_save=path_to_save)
            _, recallAt = utils_saving_result.evaluate_embedding(embeddings, labels_of_embeddings, k_list=[1])
            recallAt = recallAt[0]
            print('Test Recall@1: {}'.format(recallAt))
            utils_saving_result.append_to_text_file(path_to_save, name_of_file="test_recall_at_1", line_to_add="{} \n".format(recallAt))

            utils_saving_result.plot_embedding_of_points(embedding=embeddings, labels=labels_of_embeddings, n_samples_plot=2000, path_to_save=path_to_save, name_of_plot="test_embedding", translation_of_class_names=self.dataset_name)

    def __evaluate_test_for_fewShot_OnlyLossAndAccuracy(self, iterations, network_has_class_layer=True):
        test_accuracy_metric = tf.metrics.Accuracy()
        test_loss_metric = tf.metrics.Mean()

        itr = 0
        for tmb, lmb in self.test_dataset:
            print("test loading number: {}, number of test samples processed: {} ...".format(itr, itr*2*self.n)) #--> 2 is because we have 2 samples per class in each batch
            itr += 1
            if DEBUGGING and itr >= 5:  #--> for debugging
                break
            for task, labels in zip(tmb, lmb):
                train_ds, val_ds, train_labels, val_labels = self.get_task_train_and_val_ds(task, labels)
                updated_model = self.inner_train_loop(train_ds, train_labels, iterations)
                # If you want to compare with MAML paper, please set the training=True in the following line
                # In that paper the assumption is that we have access to all of test data together and we can evaluate
                # mean and variance from the batch which is given. Make sure to do the same thing in validation.
                updated_model_logits = updated_model(val_ds, training=True)
                if network_has_class_layer:
                    updated_model_embeddings = updated_model.get_embedding(val_ds, training=True)
                else:
                    updated_model_embeddings = updated_model_logits

                self.update_loss_and_accuracy(updated_model_logits, val_labels, test_loss_metric, test_accuracy_metric, updated_model_embeddings)

            # self.log_metric(test_summary_writer, 'Loss', test_loss_metric, step=1)
            # self.log_metric(test_summary_writer, 'Accuracy', test_accuracy_metric, step=1)

            print('Test Loss: {}'.format(test_loss_metric.result().numpy()))
            print('Test Accuracy: {}'.format(test_accuracy_metric.result().numpy()))

        path_to_save = "./saved_files/test_results/"
        utils_saving_result.append_to_text_file(path_to_save, name_of_file="test_loss", line_to_add="{} \n".format(test_loss_metric.result().numpy()))
        utils_saving_result.append_to_text_file(path_to_save, name_of_file="test_accuracy", line_to_add="{} \n".format(test_accuracy_metric.result().numpy()))

    def __evaluate_test_for_fewShot_OnlyEmbeddingAnalysis(self, iterations, network_has_class_layer=True):
        get_embedding = True
        first_embedding = True
        embeddings = None
        labels_of_embeddings = []
        itr = 0
        for tmb, lmb, real_labels in self.test_dataset_withRealLabels:
            print("test loading number: {}, number of test samples processed: {} ...".format(itr, itr*2*self.n)) #--> 2 is because we have 2 samples per class in each batch
            itr += 1
            if DEBUGGING and itr >= 5:  #--> for debugging
                break
            for task, labels, real_label in zip(tmb, lmb, real_labels):
                train_ds, val_ds, train_labels, val_labels = self.get_task_train_and_val_ds(task, labels)
                updated_model = self.inner_train_loop(train_ds, train_labels, iterations)
                # If you want to compare with MAML paper, please set the training=True in the following line
                # In that paper the assumption is that we have access to all of test data together and we can evaluate
                # mean and variance from the batch which is given. Make sure to do the same thing in validation.
                updated_model_logits = updated_model(val_ds, training=True)
                
                # get the embeddings:
                if get_embedding:
                    if network_has_class_layer:
                        embedding1 = updated_model.get_embedding(train_ds, training=True)
                        embedding2 = updated_model.get_embedding(val_ds, training=True)
                    else:
                        embedding1 = updated_model(train_ds, training=True)
                        embedding2 = updated_model_logits
                    embedding1 = embedding1.numpy()
                    embedding2 = embedding2.numpy()
                    # Note: train_labels are the fake labels for n-way in meta-learning
                    # extract real label from the image path:
                    embedding1_labels = real_label[0,:,:].numpy()
                    embedding1_labels = embedding1_labels.ravel()
                    embedding1_labels = [str(embedding1_labels[i]).split("\\")[-1].split("/")[0] for i in range(len(embedding1_labels))]
                    embedding2_labels = embedding1_labels   #--> The labels of the second set in pairs is equal to the labels of the first set of points in the pairs.
                    if first_embedding:
                        first_embedding = False
                        embeddings = embedding1
                    else:
                        embeddings = np.vstack((embeddings, embedding1))
                    embeddings = np.vstack((embeddings, embedding2))
                    labels_of_embeddings.extend(embedding1_labels)
                    labels_of_embeddings.extend(embedding2_labels)

        path_to_save = "./saved_files/test_results/"
        if get_embedding:
            utils_saving_result.save_variable(variable=embeddings, name_of_variable="embeddings", path_to_save=path_to_save)
            utils_saving_result.save_variable(variable=labels_of_embeddings, name_of_variable="labels_of_embeddings", path_to_save=path_to_save)
            _, recallAt = utils_saving_result.evaluate_embedding(embeddings, labels_of_embeddings, k_list=[1])
            recallAt = recallAt[0]
            print('Test Recall@1: {}'.format(recallAt))
            utils_saving_result.append_to_text_file(path_to_save, name_of_file="test_recall_at_1", line_to_add="{} \n".format(recallAt))

            utils_saving_result.plot_embedding_of_points(embedding=embeddings, labels=labels_of_embeddings, n_samples_plot=2000, path_to_save=path_to_save, name_of_plot="test_embedding", translation_of_class_names=self.dataset_name)

    def report_validation_loss_and_accuracy_1Shot(self, epoch_count, evaluate_embedding=True, network_has_class_layer=True):
        self.val_loss_metric.reset_states()
        self.val_accuracy_metric.reset_states()

        if evaluate_embedding:
            embedding_dim = 128
            val_embeddings = np.zeros((0, embedding_dim))
            val_labels_list = []
            if self.dataset_name == "mini_imagenet":
                val_class_names = ["n01855672", "n02091244", "n02114548", "n02138441", "n02174001", "n02950826", "n02971356", "n02981792", "n03075370", "n03417042", "n03535780", "n03584254", "n03770439", "n03773504", "n03980874", "n09256479"]
            elif self.dataset_name == "CRC" or self.dataset_name == "CRC_cropped":
                val_class_names = ["00_TUMOR", "01_STROMA", "02_MUCUS", "03_LYMPHO", "04_DEBRIS", "05_SMOOTH_MUSCLE", "06_ADIPOSE", "07_BACKGROUND", "08_NORMAL"]
        val_counter = 0
        for tmb, lmb, real_labels in self.val_dataset_withRealLabels:
            if val_counter % 20 == 0:
                print("val counter: {}".format(val_counter))
            val_counter += 1
            for task, labels, real_label in zip(tmb, lmb, real_labels):
                train_ds, val_ds, train_labels, val_labels = self.get_task_train_and_val_ds(task, labels)
                if val_counter % 5 == 0:
                    step = epoch_count * self.val_dataset_withRealLabels.steps_per_epoch + val_counter
                    self.log_images(self.val_summary_writer, train_ds, val_ds, step)

                updated_model = self.inner_train_loop(train_ds, train_labels, self.num_steps_validation)
                # If you want to compare with MAML paper, please set the training=True in the following line
                # In that paper the assumption is that we have access to all of test data together and we can evaluate
                # mean and variance from the batch which is given. Make sure to do the same thing in evaluation.
                updated_model_logits = updated_model(val_ds, training=True)
                if network_has_class_layer:
                    updated_model_embedding = updated_model.get_embedding(val_ds, training=True)
                else:
                    updated_model_embedding = updated_model_logits

                self.update_loss_and_accuracy(
                    updated_model_logits, val_labels, self.val_loss_metric, self.val_accuracy_metric, updated_model_embedding
                )

                if evaluate_embedding:
                    embedding1_labels = real_label[0,:,:].numpy()
                    embedding1_labels = embedding1_labels.ravel()
                    embedding1_labels = [str(embedding1_labels[i]).split("\\")[-1].split("/")[0] for i in range(len(embedding1_labels))]
                    embedding1_labels = [val_class_names.index(i) for i in embedding1_labels]
                    val_embeddings = np.vstack((val_embeddings, updated_model_embedding.numpy()))
                    val_labels_list.extend(embedding1_labels) 

        # self.log_metric(self.val_summary_writer, 'Loss', self.val_loss_metric, step=epoch_count)
        # self.log_metric(self.val_summary_writer, 'Accuracy', self.val_accuracy_metric, step=epoch_count)

        print('Validation Loss: {}'.format(self.val_loss_metric.result().numpy()))
        print('Validation Accuracy: {}'.format(self.val_accuracy_metric.result().numpy()))

        path_to_save = "./saved_files/val_results/"
        utils_saving_result.append_to_text_file(path_to_save, name_of_file="val_loss", line_to_add="{} \t {} \n".format(epoch_count, self.val_loss_metric.result().numpy()))
        utils_saving_result.append_to_text_file(path_to_save, name_of_file="val_accuracy", line_to_add="{} \t {} \n".format(epoch_count, self.val_accuracy_metric.result().numpy()))

        if evaluate_embedding:
            _, recallAt = utils_saving_result.evaluate_embedding(val_embeddings, val_labels_list, k_list=[1])
            recallAt = recallAt[0]
            print('Validation Recall@1: {}'.format(recallAt))
            utils_saving_result.append_to_text_file(path_to_save, name_of_file="val_recall_at_1", line_to_add="{} \t {} \n".format(epoch_count, recallAt))

            utils_saving_result.plot_embedding_of_points(embedding=val_embeddings, labels=val_labels_list, n_samples_plot=2000, path_to_save=path_to_save+"val_embeddings/", name_of_plot="epoch{}".format(epoch_count))

    def report_validation_loss_and_accuracy_fewShot_OnlyLossAndAccuracy(self, epoch_count):
        # in few shot (other than 1 shot), we embded all data points together (for recall@) (for embedding we feed to network 1-shot because we don't care about shot) 
        # and calculate accuracy/loss using few-shot
        self.val_loss_metric.reset_states()
        self.val_accuracy_metric.reset_states()

        val_counter = 0
        for tmb, lmb in self.val_dataset:
            if val_counter % 20 == 0:
                print("val counter: {}".format(val_counter))
            val_counter += 1
            for task, labels in zip(tmb, lmb):
                train_ds, val_ds, train_labels, val_labels = self.get_task_train_and_val_ds(task, labels)
                if val_counter % 5 == 0:
                    step = epoch_count * self.val_dataset.steps_per_epoch + val_counter
                    self.log_images(self.val_summary_writer, train_ds, val_ds, step)

                updated_model = self.inner_train_loop(train_ds, train_labels, self.num_steps_validation)
                # If you want to compare with MAML paper, please set the training=True in the following line
                # In that paper the assumption is that we have access to all of test data together and we can evaluate
                # mean and variance from the batch which is given. Make sure to do the same thing in evaluation.
                updated_model_logits = updated_model(val_ds, training=True)
                if network_has_class_layer:
                    updated_model_embedding = updated_model.get_embedding(val_ds, training=True)
                else:
                    updated_model_embedding = updated_model_logits

                self.update_loss_and_accuracy(
                    updated_model_logits, val_labels, self.val_loss_metric, self.val_accuracy_metric, updated_model_embedding
                )

        # self.log_metric(self.val_summary_writer, 'Loss', self.val_loss_metric, step=epoch_count)
        # self.log_metric(self.val_summary_writer, 'Accuracy', self.val_accuracy_metric, step=epoch_count)

        print('Validation Loss: {}'.format(self.val_loss_metric.result().numpy()))
        print('Validation Accuracy: {}'.format(self.val_accuracy_metric.result().numpy()))

        path_to_save = "./saved_files/val_results/"
        utils_saving_result.append_to_text_file(path_to_save, name_of_file="val_loss", line_to_add="{} \t {} \n".format(epoch_count, self.val_loss_metric.result().numpy()))
        utils_saving_result.append_to_text_file(path_to_save, name_of_file="val_accuracy", line_to_add="{} \t {} \n".format(epoch_count, self.val_accuracy_metric.result().numpy()))

    def report_validation_loss_and_accuracy_fewShot_OnlyEmbeddingAnalysis(self, epoch_count, network_has_class_layer=True):
        # in few shot (other than 1 shot), we embded all data points together (for recall@) (for embedding we feed to network 1-shot because we don't care about shot) 
        # and calculate accuracy/loss using few-shot
        evaluate_embedding = True
        if evaluate_embedding:
            embedding_dim = 128
            val_embeddings = np.zeros((0, embedding_dim))
            val_labels_list = []
            if self.dataset_name == "mini_imagenet":
                val_class_names = ["n01855672", "n02091244", "n02114548", "n02138441", "n02174001", "n02950826", "n02971356", "n02981792", "n03075370", "n03417042", "n03535780", "n03584254", "n03770439", "n03773504", "n03980874", "n09256479"]
            elif self.dataset_name == "CRC" or self.dataset_name == "CRC_cropped":
                val_class_names = ["00_TUMOR", "01_STROMA", "02_MUCUS", "03_LYMPHO", "04_DEBRIS", "05_SMOOTH_MUSCLE", "06_ADIPOSE", "07_BACKGROUND", "08_NORMAL"]
        val_counter = 0
        for tmb, lmb, real_labels in self.val_dataset_withRealLabels:
            if val_counter % 20 == 0:
                print("val counter: {}".format(val_counter))
            val_counter += 1
            for task, labels, real_label in zip(tmb, lmb, real_labels):
                train_ds, val_ds, train_labels, val_labels = self.get_task_train_and_val_ds(task, labels)
                if val_counter % 5 == 0:
                    step = epoch_count * self.val_dataset.steps_per_epoch + val_counter
                    self.log_images(self.val_summary_writer, train_ds, val_ds, step)

                updated_model = self.inner_train_loop(train_ds, train_labels, self.num_steps_validation)
                # If you want to compare with MAML paper, please set the training=True in the following line
                # In that paper the assumption is that we have access to all of test data together and we can evaluate
                # mean and variance from the batch which is given. Make sure to do the same thing in evaluation.
                updated_model_logits = updated_model(val_ds, training=True)
                if network_has_class_layer:
                    updated_model_embedding = updated_model.get_embedding(val_ds, training=True)
                else:
                    updated_model_embedding = updated_model_logits

                if evaluate_embedding:
                    embedding1_labels = real_label[0,:,:].numpy()
                    embedding1_labels = embedding1_labels.ravel()
                    embedding1_labels = [str(embedding1_labels[i]).split("\\")[-1].split("/")[0] for i in range(len(embedding1_labels))]
                    embedding1_labels = [val_class_names.index(i) for i in embedding1_labels]
                    val_embeddings = np.vstack((val_embeddings, updated_model_embedding.numpy()))
                    val_labels_list.extend(embedding1_labels) 

        path_to_save = "./saved_files/val_results/"
        if evaluate_embedding:
            _, recallAt = utils_saving_result.evaluate_embedding(val_embeddings, val_labels_list, k_list=[1])
            recallAt = recallAt[0]
            print('Validation Recall@1: {}'.format(recallAt))
            utils_saving_result.append_to_text_file(path_to_save, name_of_file="val_recall_at_1", line_to_add="{} \t {} \n".format(epoch_count, recallAt))

            utils_saving_result.plot_embedding_of_points(embedding=val_embeddings, labels=val_labels_list, n_samples_plot=2000, path_to_save=path_to_save+"val_embeddings/", name_of_plot="epoch{}".format(epoch_count))

    # @tf.function
    def get_losses_of_tasks_batch(self, inputs):
        task, labels, iteration_count = inputs

        train_ds, val_ds, train_labels, val_labels = self.get_task_train_and_val_ds(task, labels)

        # if self.log_train_images_after_iteration != -1 and \
        #         iteration_count % self.log_train_images_after_iteration == 0:
        # aa = tf.not_equal(self.log_train_images_after_iteration, -1)
        # bb = tf.equal(iteration_count % self.log_train_images_after_iteration, 0)
        # if tf.cond(tf.logical_and(aa, bb), lambda: 1, lambda: 0):

        #     self.log_images(self.train_summary_writer, train_ds, val_ds, step=iteration_count)

        #     with tf.device('cpu:0'):
        #         with self.train_summary_writer.as_default():
        #             for var in self.model.variables:
        #                 tf.summary.histogram(var.name, var, step=iteration_count)

        #             for k in range(len(self.updated_models)):
        #                 var_count = 0
        #                 if hasattr(self.updated_models[k], 'meta_trainable_variables'):
        #                     for var in self.updated_models[k].meta_trainable_variables:
        #                         var_count += 1
        #                         tf.summary.histogram(f'updated_model_{k}_' + str(var_count), var, step=iteration_count)

        updated_model = self.inner_train_loop(train_ds, train_labels, -1)
        updated_model_logits = updated_model(val_ds, training=True)
        val_loss = tf.reduce_sum(
            tf.losses.categorical_crossentropy(val_labels, updated_model_logits, from_logits=True)
        )
        self.train_loss_metric.update_state(val_loss)
        self.train_accuracy_metric.update_state(
            tf.argmax(val_labels, axis=-1),
            tf.argmax(updated_model_logits, axis=-1)
        )
        return val_loss

    def meta_train_loop(self, tasks_meta_batch, labels_meta_batch, iteration_count):
        with tf.GradientTape(persistent=True) as outer_tape:
            tasks_final_losses = tf.map_fn(
                self.get_losses_of_tasks_batch,
                elems=(
                    tasks_meta_batch,
                    labels_meta_batch,
                    tf.cast(tf.ones(self.meta_batch_size, 1) * iteration_count, tf.int64)
                ),
                dtype=tf.float32,
                parallel_iterations=self.meta_batch_size
            )
            final_loss = tf.reduce_mean(tasks_final_losses)
        outer_gradients = outer_tape.gradient(final_loss, self.model.trainable_variables)
        if self.clip_gradients:
            outer_gradients = [tf.clip_by_value(grad, -10, 10) for grad in outer_gradients]
        self.optimizer.apply_gradients(zip(outer_gradients, self.model.trainable_variables))

    def train(self, epochs=5, epochs_to_load_from=None):
        self.train_dataset = self.get_train_dataset()
        if self.k == 1:
            self.val_dataset_withRealLabels = self.get_val_dataset_withRealLabels()
        else:
            self.val_dataset = self.get_val_dataset()
            self.val_dataset_withRealLabels = self.get_val_dataset_withRealLabels()
        if epochs_to_load_from is not None:
            start_epoch = self.load_model(epochs=epochs_to_load_from)
        else:
            start_epoch = 0
        iteration_count = start_epoch * self.train_dataset.steps_per_epoch

        pbar = tqdm(self.train_dataset)

        for epoch_count in range(start_epoch, epochs):
            print("epoch {} .........".format(epoch_count))
            if epoch_count != 0:
                if epoch_count % self.report_validation_frequency == 0:
                    print("Evaluating the validation performance...")
                    if self.k == 1:
                        self.report_validation_loss_and_accuracy_1Shot(epoch_count, evaluate_embedding=True, network_has_class_layer=True)
                    else:
                        self.report_validation_loss_and_accuracy_fewShot_OnlyLossAndAccuracy(epoch_count)
                        self.report_validation_loss_and_accuracy_fewShot_OnlyEmbeddingAnalysis(epoch_count, network_has_class_layer=True)
                    print('Train Loss: {}'.format(self.train_loss_metric.result().numpy()))
                    print('Train Accuracy: {}'.format(self.train_accuracy_metric.result().numpy()))
                    utils_saving_result.append_to_text_file(path_to_save="./saved_files/val_results/", name_of_file="train_loss", line_to_add="{} \t {} \n".format(epoch_count, self.train_loss_metric.result().numpy()))
                    utils_saving_result.append_to_text_file(path_to_save="./saved_files/val_results/", name_of_file="train_accuracy", line_to_add="{} \t {} \n".format(epoch_count, self.train_accuracy_metric.result().numpy()))

                with self.train_summary_writer.as_default():
                    tf.summary.scalar('Loss', self.train_loss_metric.result(), step=epoch_count)
                    tf.summary.scalar('Accuracy', self.train_accuracy_metric.result(), step=epoch_count)

                if epoch_count % self.save_after_epochs == 0:
                    self.save_model(epoch_count)

            self.train_accuracy_metric.reset_states()
            self.train_loss_metric.reset_states()

            for tasks_meta_batch, labels_meta_batch in self.train_dataset:
                self.meta_train_loop(tasks_meta_batch, labels_meta_batch, iteration_count)
                iteration_count += 1
                pbar.set_description_str('Epoch{}/{}, Iteration{}/{}: Train Loss: {}, Train Accuracy: {}'.format(
                    epoch_count,
                    epochs,
                    iteration_count,
                    self.train_dataset.steps_per_epoch,
                    self.train_loss_metric.result().numpy(),
                    self.train_accuracy_metric.result().numpy()
                ))
                pbar.update(1)
                if DEBUGGING and iteration_count >= 1:  #--> for debugging
                    break


def run_omniglot(n_way=5, k_shot=1, do_train=True, epoch_to_load_for_test=1, epoch_to_load_for_train=None,
                      epochs_training=100, iterations_test=50, meta_batch_size=32, get_embedding_in_test_phase=False):
    omniglot_database = OmniglotDatabase(
        random_seed=47,
        num_train_classes=1200,
        num_val_classes=100,
    )

    maml = ModelAgnosticMetaLearningModel(
        database=omniglot_database,
        network_cls=SimpleModel,
        n=n_way,
        k=k_shot,
        meta_batch_size=meta_batch_size,
        num_steps_ml=10,
        lr_inner_ml=0.4,
        num_steps_validation=10,
        save_after_epochs=1,
        meta_learning_rate=0.001,
        report_validation_frequency=1,
        log_train_images_after_iteration=-1,
        dataset_name="omniglot"
    )

    if do_train:
        maml.train(epochs=epochs_training, epochs_to_load_from=epoch_to_load_for_train)
    else:
        maml.evaluate(iterations=iterations_test, epochs_to_load_from=epoch_to_load_for_test, get_embedding=get_embedding_in_test_phase, network_has_class_layer=True)

def run_mini_imagenet(n_way=5, k_shot=1, do_train=True, epoch_to_load_for_test=1, epoch_to_load_for_train=None, 
                      epochs_training=100, iterations_test=50, meta_batch_size=4, get_embedding_in_test_phase=False):
    mini_imagenet_database = MiniImagenetDatabase(random_seed=-1)

    maml = ModelAgnosticMetaLearningModel(
        database=mini_imagenet_database,
        network_cls=MiniImagenetModel,
        n=n_way,
        k=k_shot,
        meta_batch_size=meta_batch_size,
        num_steps_ml=5,
        lr_inner_ml=0.01,
        num_steps_validation=5,
        save_after_epochs=1,
        meta_learning_rate=0.001,
        report_validation_frequency=1,
        log_train_images_after_iteration=1000,
        least_number_of_tasks_val_test=50,
        clip_gradients=True,
        dataset_name="mini_imagenet"
    )

    if do_train:
        maml.train(epochs=epochs_training, epochs_to_load_from=epoch_to_load_for_train)
    else:
        maml.evaluate(iterations=iterations_test, epochs_to_load_from=epoch_to_load_for_test, get_embedding=get_embedding_in_test_phase, network_has_class_layer=True)

def run_CRC(n_way=5, k_shot=1, do_train=True, epoch_to_load_for_test=1, epoch_to_load_for_train=None, 
                      epochs_training=100, iterations_test=50, meta_batch_size=4, get_embedding_in_test_phase=False):
    CRC_database = CRCDatabase(random_seed=-1)

    maml = ModelAgnosticMetaLearningModel(
        database=CRC_database,
        network_cls=CRCModel,
        n=n_way,
        k=k_shot,
        meta_batch_size=meta_batch_size,
        num_steps_ml=5,
        lr_inner_ml=0.005,
        num_steps_validation=5,
        save_after_epochs=1,
        meta_learning_rate=0.001,
        report_validation_frequency=1,
        log_train_images_after_iteration=1000,
        least_number_of_tasks_val_test=50,
        clip_gradients=True,
        dataset_name="CRC"
    )

    if do_train:
        maml.train(epochs=epochs_training, epochs_to_load_from=epoch_to_load_for_train)
    else:
        maml.evaluate(iterations=iterations_test, epochs_to_load_from=epoch_to_load_for_test, get_embedding=get_embedding_in_test_phase, network_has_class_layer=True)

def run_CRC_cropped(n_way=5, k_shot=1, do_train=True, epoch_to_load_for_test=1, epoch_to_load_for_train=None, 
                      epochs_training=100, iterations_test=50, meta_batch_size=4, get_embedding_in_test_phase=False):
    crcDatabase_cropped = CRCDatabase_cropped(random_seed=-1)

    maml = ModelAgnosticMetaLearningModel(
        database=crcDatabase_cropped,
        network_cls=MiniImagenetModel,
        n=n_way,
        k=k_shot,
        meta_batch_size=meta_batch_size,
        num_steps_ml=5,
        lr_inner_ml=0.01,
        num_steps_validation=5,
        save_after_epochs=1,
        meta_learning_rate=0.001,
        report_validation_frequency=1,
        log_train_images_after_iteration=1000,
        least_number_of_tasks_val_test=50,
        clip_gradients=True,
        dataset_name="CRC_cropped"
    )

    if do_train:
        maml.train(epochs=epochs_training, epochs_to_load_from=epoch_to_load_for_train)
    else:
        maml.evaluate(iterations=iterations_test, epochs_to_load_from=epoch_to_load_for_test, get_embedding=get_embedding_in_test_phase, network_has_class_layer=True)


if __name__ == '__main__':
    # settings:
    #---> Note: if you want to debug, make True the flags DEBUGGING and/or DEBUGGING_PLOT_INPUT above pages maml.py and maml_lossEmbedding.py
    do_train = True  #--> True: train, False: test
    dataset = "mini_imagenet"  #--> omniglot, mini_imagenet, CRC, CRC_cropped
    n_way = 5
    k_shot = 1  #--> number of shots in test and validation phases. Note that in UMTRA, training is always 1-shot (2-shot if considering augmentation, too)
    epoch_to_load_for_test = 1
    epoch_to_load_for_train = None  #--> if None, it trains from scratch
    epochs_training = 40  #--> 4000 for mini-imagenet, 4000 for omniglot
    iterations_test = 50  #--> 50 for mini-imagenet, 50 for omniglot
    meta_batch_size = 4  #--> 4 for mini-imagenet, 32 for omniglot
    get_embedding_in_test_phase = True

    if dataset == "omniglot":
        run_omniglot(n_way, k_shot, do_train, epoch_to_load_for_test, epoch_to_load_for_train, epochs_training, iterations_test, meta_batch_size, get_embedding_in_test_phase)
    elif dataset == "mini_imagenet":
        run_mini_imagenet(n_way, k_shot, do_train, epoch_to_load_for_test, epoch_to_load_for_train, epochs_training, iterations_test, meta_batch_size, get_embedding_in_test_phase)
    elif dataset == "CRC":
        run_CRC(n_way, k_shot, do_train, epoch_to_load_for_test, epoch_to_load_for_train, epochs_training, iterations_test, meta_batch_size, get_embedding_in_test_phase)
    elif dataset == "CRC_cropped":
        run_CRC_cropped(n_way, k_shot, do_train, epoch_to_load_for_test, epoch_to_load_for_train, epochs_training, iterations_test, meta_batch_size, get_embedding_in_test_phase)