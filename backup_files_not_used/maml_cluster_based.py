import os

import tensorflow as tf
import numpy as np
from tqdm import tqdm

from tf_datasets import OmniglotDatabase, MiniImagenetDatabase
from networks import SimpleModel, MiniImagenetModel
from maml import ModelAgnosticMetaLearningModel
from utils import combine_first_two_axes, average_gradients
import settings


class ModelAgnosticMetaLearningModel_ClusterBased(ModelAgnosticMetaLearningModel):
    def __init__(
        self,
        database,
        network_cls,
        n,
        k,
        meta_batch_size,
        num_steps_ml,
        lr_inner_ml,
        num_steps_validation,
        save_after_epochs,
        meta_learning_rate,
        report_validation_frequency,
        log_train_images_after_iteration,  # Set to -1 if you do not want to log train images.
        least_number_of_tasks_val_test=-1,  # Make sure the validaiton and test dataset pick at least this many tasks.
        clip_gradients=False,
        k_means_n_iterations=1000
    ):
        super(ModelAgnosticMetaLearningModel_ClusterBased, self).__init__(
            database=database,
            network_cls=network_cls,
            n=n,
            k=1,
            meta_batch_size=meta_batch_size,
            num_steps_ml=num_steps_ml,
            lr_inner_ml=lr_inner_ml,
            num_steps_validation=num_steps_validation,
            save_after_epochs=save_after_epochs,
            meta_learning_rate=meta_learning_rate,
            report_validation_frequency=report_validation_frequency,
            log_train_images_after_iteration=log_train_images_after_iteration,
            least_number_of_tasks_val_test=least_number_of_tasks_val_test,
            clip_gradients=clip_gradients
        )
        self.k_means_n_iterations = k_means_n_iterations

    # def get_train_loss_and_gradients(self, train_ds, train_labels):
    #     with tf.GradientTape(persistent=True) as train_tape:
    #         # TODO compare between model.forward(train_ds) and model(train_ds)
    #         logits = self.model(train_ds, training=True)
    #         # train_loss = tf.reduce_sum(tf.losses.categorical_crossentropy(train_labels, logits, from_logits=True))
    #         embedding = self.model.get_embedding(train_ds, training=True)
    #         train_loss, _ = self.cluster_based_loss(embedding=embedding, logits=logits)
    #     train_gradients = train_tape.gradient(train_loss, self.model.trainable_variables)
    #     return train_loss, train_gradients, train_tape

    def update_loss_and_accuracy(self, embeddings, logits, labels, loss_metric, accuracy_metric):
        #### TODO implement it
        )

    def inner_train_loop_withAllBatchSamples(self, train_ds, train_labels, num_iterations=-1):
        if num_iterations == -1:
            num_iterations = self.num_steps_ml

            gradients = list()
            for variable in self.model.trainable_variables:
                gradients.append(tf.zeros_like(variable))

            self.create_meta_model(self.updated_models[0], self.model, gradients)

            for k in range(1, num_iterations + 1):
                with tf.GradientTape(persistent=True) as train_tape:
                    train_tape.watch(self.updated_models[k - 1].meta_trainable_variables)
                    logits = self.updated_models[k - 1](train_ds, training=True)
                    embedding = self.updated_models[k - 1].get_embedding(train_ds, training=True)
                    # loss = tf.reduce_sum(
                    #     tf.losses.categorical_crossentropy(train_labels, logits, from_logits=True)
                    # )
                    loss, _ = self.cluster_based_loss(embedding=embedding, logits=logits)
                gradients = train_tape.gradient(loss, self.updated_models[k - 1].meta_trainable_variables)
                self.create_meta_model(self.updated_models[k], self.updated_models[k - 1], gradients)

            return self.updated_models[-1]

        else:
            gradients = list()
            for variable in self.model.trainable_variables:
                gradients.append(tf.zeros_like(variable))

            self.create_meta_model(self.updated_models[0], self.model, gradients)
            copy_model = self.updated_models[0]

            for k in range(num_iterations):
                with tf.GradientTape(persistent=True) as train_tape:
                    train_tape.watch(copy_model.meta_trainable_variables)
                    logits = copy_model(train_ds, training=True)
                    embedding = copy_model.get_embedding(train_ds, training=True)
                    # loss = tf.reduce_sum(
                    #     tf.losses.categorical_crossentropy(train_labels, logits, from_logits=True)
                    # )
                    loss, _ = self.cluster_based_loss(embedding=embedding, logits=logits)
                gradients = train_tape.gradient(loss, copy_model.meta_trainable_variables)
                self.create_meta_model(copy_model, copy_model, gradients)

            return copy_model

    def cluster_based_loss(self, embedding, logits):
        # this loss replaces tf.losses.categorical_crossentropy in this code
        # shape of train_ds: [80, 84, 84, 3] --> [batch_size * n_way, n_rows of image, n_cols of image, n_channels of image]
        # shape of logits: [80, 5]
        # shape of embedding: [80, dim_embedding=288] --> [batch_size * n_way, embedding dimensionality]
        n_way = tf.shape(logits)[1]
        n_points = tf.shape(logits)[0]
        #--------------- k-means clustering:
        cluster_assignments = self.K_means(points=embedding, K=n_way, MAX_ITERS=self.k_means_n_iterations)
        cluster_assignments = tf.reshape(cluster_assignments, shape=[n_points, 1])
        cluster_assignments = tf.one_hot(indices=cluster_assignments, depth=n_way)
        cluster_assignments = tf.reshape(cluster_assignments, shape=[n_points, n_way])
        #--------------- cross entropy loss:
        loss = tf.reduce_sum(tf.losses.categorical_crossentropy(y_true=cluster_assignments, y_pred=logits, 
                                                                from_logits=True))
        return loss, cluster_assignments

    def K_means(self, points, K, MAX_ITERS=1000):
        # https://stackoverflow.com/questions/33621643/how-would-i-implement-k-means-with-tensorflow
        # points --> shape: (n_points, n_features)
        N = tf.shape(points)[0]
        n_features = tf.shape(points)[1]

        # Silly initialization: Use the first K points as the starting centroids.
        # centroids = tf.Variable(tf.slice(points, [0,0], [K,-1])) # shape of centroids --> (K, n_features)

        # random initialization:
        indices_centeroids = np.random.choice(range(N), K, replace=False)
        indices_centeroids_tf = tf.constant(indices_centeroids)
        centroids = tf.gather(points, indices=indices_centeroids_tf, axis=0)

        # Replicate to N copies of each centroid and K copies of each point, then subtract and compute the sum of squared distances.   
        rep_points = tf.reshape(tf.tile(points, [1, K]), [N, K, n_features])
        iters = 0
        while iters < MAX_ITERS: 
            iters += 1     
            rep_centroids = tf.reshape(tf.tile(centroids, [N, 1]), [N, K, n_features])
            distances_points_from_centroids = tf.reduce_sum(tf.square(rep_points - rep_centroids), axis=2)
            # shape of distances_points_from_centroids --> (N, K)
            cluster_assignments = tf.argmin(distances_points_from_centroids, 1)
        return cluster_assignments

    def get_task_train_and_val_ds_withAllBatchSamples(self, task, labels):
        train_ds, val_ds = tf.split(task, num_or_size_splits=2, axis=1)
        train_labels, val_labels = tf.split(labels, num_or_size_splits=2, axis=1)
        
        train_ds = combine_first_two_axes(tf.squeeze(train_ds, axis=[1,3]))
        val_ds = combine_first_two_axes(tf.squeeze(val_ds, axis=[1,3]))
        train_labels = combine_first_two_axes(tf.squeeze(train_labels, axis=[1,3]))
        val_labels = combine_first_two_axes(tf.squeeze(val_labels, axis=[1,3]))

        return train_ds, val_ds, train_labels, val_labels

    def get_losses_of_tasks_batch_withAllBatchSamples(self, inputs):
        task, labels, iteration_count = inputs
        train_ds, val_ds, train_labels, val_labels = self.get_task_train_and_val_ds_withAllBatchSamples(task, labels)

        updated_model = self.inner_train_loop_withAllBatchSamples(train_ds, train_labels, -1)
        updated_model_logits = updated_model(val_ds, training=True)
        # val_loss = tf.reduce_sum(
        #     tf.losses.categorical_crossentropy(val_labels, updated_model_logits, from_logits=True)
        # )
        embedding = updated_model.get_embedding(val_ds, training=True)
        val_loss, val_labels = self.cluster_based_loss(embedding=embedding, logits=updated_model_logits)
        self.train_loss_metric.update_state(val_loss)
        self.train_accuracy_metric.update_state(
            tf.argmax(val_labels, axis=-1),
            tf.argmax(updated_model_logits, axis=-1)
        )
        return val_loss

    def meta_train_loop(self, tasks_meta_batch, labels_meta_batch, iteration_count):
        with tf.GradientTape(persistent=True) as outer_tape:
            tasks_final_losses = self.get_losses_of_tasks_batch_withAllBatchSamples(inputs=( tasks_meta_batch, labels_meta_batch, tf.cast(tf.ones(self.meta_batch_size, 1) * iteration_count, tf.int64) ))
            final_loss = tf.reduce_mean(tasks_final_losses)
        outer_gradients = outer_tape.gradient(final_loss, self.model.trainable_variables)
        if self.clip_gradients:
            outer_gradients = [tf.clip_by_value(grad, -10, 10) for grad in outer_gradients]
        self.optimizer.apply_gradients(zip(outer_gradients, self.model.trainable_variables))

    def train(self, epochs=5, epochs_to_load_from=None):
        self.train_dataset = self.get_train_dataset()
        self.val_dataset = self.get_val_dataset()  #--> This is the validation set of dataset (not the val data for meta-train). It is not batch-wise (is fed to network one by one)
        if epochs_to_load_from is not None:
            start_epoch = self.load_model(epochs=epochs_to_load_from)
        else:
            start_epoch = 0
        iteration_count = start_epoch * self.train_dataset.steps_per_epoch

        pbar = tqdm(self.train_dataset)

        for epoch_count in range(start_epoch, epochs):
            print("epoch {} .........".format(epoch_count))
            
            if epoch_count != 0:
                if epoch_count % self.report_validation_frequency == 0:
                    self.report_validation_loss_and_accuracy(epoch_count)
                    if epoch_count != 0:
                        print('Train Loss: {}'.format(self.train_loss_metric.result().numpy()))
                        print('Train Accuracy: {}'.format(self.train_accuracy_metric.result().numpy()))

                with self.train_summary_writer.as_default():
                    tf.summary.scalar('Loss', self.train_loss_metric.result(), step=epoch_count)
                    tf.summary.scalar('Accuracy', self.train_accuracy_metric.result(), step=epoch_count)

                if epoch_count % self.save_after_epochs == 0:
                    self.save_model(epoch_count)

            self.train_accuracy_metric.reset_states()
            self.train_loss_metric.reset_states()

            for tasks_meta_batch, labels_meta_batch in self.train_dataset:
                self.meta_train_loop(tasks_meta_batch, labels_meta_batch, iteration_count)
                iteration_count += 1
                pbar.set_description_str('Epoch{}/{}, Iteration{}/{}: Train Loss: {}, Train Accuracy: {}'.format(
                    epoch_count,
                    epochs,
                    iteration_count,
                    self.train_dataset.steps_per_epoch,
                    self.train_loss_metric.result().numpy(),
                    self.train_accuracy_metric.result().numpy()
                ))
                pbar.update(1)
