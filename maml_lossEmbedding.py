import os
import utils.utils_saving_result as utils_saving_result

import tensorflow as tf
import numpy as np
from tqdm import tqdm

from utils.tf_datasets import OmniglotDatabase, MiniImagenetDatabase, CRCDatabase, CRCDatabase_cropped
from utils.networks_noClassLayer import SimpleModel as SimpleModel_noClassLayer, MiniImagenetModel as MiniImagenetModel_noClassLayer, CRCModel as CRCModel_noClassLayer
from utils.networks import SimpleModel, MiniImagenetModel, CRCModel
from maml import ModelAgnosticMetaLearningModel
from utils.utils import combine_first_two_axes, average_gradients
import utils.settings as settings
import utils.utils_loss as utils_loss

DEBUGGING = False

class ModelAgnosticMetaLearningModel_lossEmbedding(ModelAgnosticMetaLearningModel):
    def __init__(
        self,
        database,
        network_cls,
        n,
        k,
        meta_batch_size,
        num_steps_ml,
        lr_inner_ml,
        num_steps_validation,
        save_after_epochs,
        meta_learning_rate,
        report_validation_frequency,
        log_train_images_after_iteration,  # Set to -1 if you do not want to log train images.
        least_number_of_tasks_val_test=-1,  # Make sure the validaiton and test dataset pick at least this many tasks.
        clip_gradients=False,
        loss_type="cross_entropy",
        dataset_name="",
        beta1_triplet=1, 
        beta2_crossEntropy=1
    ):
        super(ModelAgnosticMetaLearningModel_lossEmbedding, self).__init__(
            database=database,
            network_cls=network_cls,
            n=n,
            k=k,
            meta_batch_size=meta_batch_size,
            num_steps_ml=num_steps_ml,
            lr_inner_ml=lr_inner_ml,
            num_steps_validation=num_steps_validation,
            save_after_epochs=save_after_epochs,
            meta_learning_rate=meta_learning_rate,
            report_validation_frequency=report_validation_frequency,
            log_train_images_after_iteration=log_train_images_after_iteration,
            least_number_of_tasks_val_test=least_number_of_tasks_val_test,
            clip_gradients=clip_gradients,
            dataset_name=dataset_name
        )
        self.loss_type = loss_type
        self.margin = 10  #--> margin in triplet loss
        if dataset_name == "":
            raise ValueError("A valid dataset name should be given")
        self.dataset_name = dataset_name
        self.beta1_triplet = beta1_triplet 
        self.beta2_crossEntropy = beta2_crossEntropy

    def inner_train_loop(self, train_ds, train_labels, num_iterations=-1):
        if num_iterations == -1:
            num_iterations = self.num_steps_ml

            gradients = list()
            for variable in self.model.trainable_variables:
                gradients.append(tf.zeros_like(variable))

            self.create_meta_model(self.updated_models[0], self.model, gradients)

            for k in range(1, num_iterations + 1):
                with tf.GradientTape(persistent=True) as train_tape:
                    train_tape.watch(self.updated_models[k - 1].meta_trainable_variables)
                    logits = self.updated_models[k - 1](train_ds, training=True)
                    if self.loss_type == "batch_hard_triplet":
                        loss = tf.reduce_sum(
                            utils_loss.batch_hard_triplet_loss(labels=train_labels, embeddings=logits, margin=self.margin, squared=True)
                        )
                    elif self.loss_type == "crossEntropy_and_triplet":
                        embeddings = self.updated_models[k - 1].get_embedding(train_ds, training=True)
                        loss = tf.reduce_sum(
                            utils_loss.batchHardTriplet_and_crossEntropy_loss(labels=train_labels, embeddings=embeddings, logits=logits, margin=self.margin, squared=True, beta1_triplet=self.beta1_triplet, beta2_crossEntropy=self.beta2_crossEntropy)
                        )
                gradients = train_tape.gradient(loss, self.updated_models[k - 1].meta_trainable_variables)
                self.create_meta_model(self.updated_models[k], self.updated_models[k - 1], gradients)

            return self.updated_models[-1]

        else:
            gradients = list()
            for variable in self.model.trainable_variables:
                gradients.append(tf.zeros_like(variable))

            self.create_meta_model(self.updated_models[0], self.model, gradients)
            copy_model = self.updated_models[0]

            for k in range(num_iterations):
                with tf.GradientTape(persistent=True) as train_tape:
                    train_tape.watch(copy_model.meta_trainable_variables)
                    logits = copy_model(train_ds, training=True)
                    if self.loss_type == "batch_hard_triplet":
                        loss = tf.reduce_sum(
                            utils_loss.batch_hard_triplet_loss(labels=train_labels, embeddings=logits, margin=self.margin, squared=True)
                        )
                    elif self.loss_type == "crossEntropy_and_triplet":
                        embeddings = copy_model.get_embedding(train_ds, training=True)
                        loss = tf.reduce_sum(
                            utils_loss.batchHardTriplet_and_crossEntropy_loss(labels=train_labels, embeddings=embeddings, logits=logits, margin=self.margin, squared=True, beta1_triplet=self.beta1_triplet, beta2_crossEntropy=self.beta2_crossEntropy)
                        )
                gradients = train_tape.gradient(loss, copy_model.meta_trainable_variables)
                self.create_meta_model(copy_model, copy_model, gradients)

            return copy_model

    def update_loss_and_accuracy(self, logits, labels, loss_metric, accuracy_metric, embeddings=None):
        # print(tf.argmax(logits, axis=-1))
        if self.loss_type == "batch_hard_triplet":
            val_loss = tf.reduce_sum(
                utils_loss.batch_hard_triplet_loss(labels=labels, embeddings=logits, margin=self.margin, squared=True)
            )
        elif self.loss_type == "crossEntropy_and_triplet":
            val_loss = tf.reduce_sum(
                utils_loss.batchHardTriplet_and_crossEntropy_loss(labels=labels, embeddings=embeddings, logits=logits, margin=self.margin, squared=True, beta1_triplet=self.beta1_triplet, beta2_crossEntropy=self.beta2_crossEntropy)
            )
        loss_metric.update_state(val_loss)
        accuracy_metric.update_state(
            tf.argmax(labels, axis=-1),
            tf.argmax(logits, axis=-1)
        )

    # @tf.function
    def get_losses_of_tasks_batch(self, inputs):
        task, labels, iteration_count = inputs

        train_ds, val_ds, train_labels, val_labels = self.get_task_train_and_val_ds(task, labels)

        updated_model = self.inner_train_loop(train_ds, train_labels, -1)
        updated_model_logits = updated_model(val_ds, training=True)
        if self.loss_type == "batch_hard_triplet":
            val_loss = tf.reduce_sum(
                utils_loss.batch_hard_triplet_loss(labels=val_labels, embeddings=updated_model_logits, margin=self.margin, squared=True)
            )
        elif self.loss_type == "crossEntropy_and_triplet":
            updated_model_embeddings = updated_model.get_embedding(val_ds, training=True)
            val_loss = tf.reduce_sum(
                utils_loss.batchHardTriplet_and_crossEntropy_loss(labels=val_labels, embeddings=updated_model_embeddings, logits=updated_model_logits, margin=self.margin, squared=True, beta1_triplet=self.beta1_triplet, beta2_crossEntropy=self.beta2_crossEntropy)
            )
        self.train_loss_metric.update_state(val_loss)
        return val_loss

    def train(self, epochs=5, epochs_to_load_from=None):
        self.train_dataset = self.get_train_dataset()
        if self.k == 1:
            self.val_dataset_withRealLabels = self.get_val_dataset_withRealLabels()
        else:
            self.val_dataset = self.get_val_dataset()
            self.val_dataset_withRealLabels = self.get_val_dataset_withRealLabels()
        if epochs_to_load_from is not None:
            start_epoch = self.load_model(epochs=epochs_to_load_from)
        else:
            start_epoch = 0
        iteration_count = start_epoch * self.train_dataset.steps_per_epoch

        pbar = tqdm(self.train_dataset)

        for epoch_count in range(start_epoch, epochs):
            print("epoch {} .........".format(epoch_count))
            if epoch_count != 0:
                if epoch_count % self.report_validation_frequency == 0:
                    print("Evaluating the validation performance...")
                    if self.loss_type == "crossEntropy_and_triplet":
                        network_has_class_layer = True
                    else:
                        network_has_class_layer = False
                    if self.k == 1:
                        self.report_validation_loss_and_accuracy_1Shot(epoch_count, evaluate_embedding=True, network_has_class_layer=network_has_class_layer)
                    else:
                        self.report_validation_loss_and_accuracy_fewShot_OnlyLossAndAccuracy(epoch_count)
                        self.report_validation_loss_and_accuracy_fewShot_OnlyEmbeddingAnalysis(epoch_count, network_has_class_layer=network_has_class_layer)
                    # self.report_validation_loss_and_accuracy(epoch_count)
                    print('Train Loss: {}'.format(self.train_loss_metric.result().numpy()))
                    utils_saving_result.append_to_text_file(path_to_save="./saved_files/val_results/", name_of_file="train_loss", line_to_add="{} \t {} \n".format(epoch_count, self.train_loss_metric.result().numpy()))

                if epoch_count % self.save_after_epochs == 0:
                    self.save_model(epoch_count)

            self.train_loss_metric.reset_states()

            for tasks_meta_batch, labels_meta_batch in self.train_dataset:
                self.meta_train_loop(tasks_meta_batch, labels_meta_batch, iteration_count)
                iteration_count += 1
                pbar.set_description_str('Epoch{}/{}, Iteration{}/{}: Train Loss: {}'.format(
                    epoch_count,
                    epochs,
                    iteration_count,
                    self.train_dataset.steps_per_epoch,
                    self.train_loss_metric.result().numpy()
                ))
                pbar.update(1)
                if DEBUGGING and iteration_count >= 1:  #--> for debugging
                    break


def run_omniglot(n_way=5, k_shot=1, do_train=True, epoch_to_load_for_test=1, epoch_to_load_for_train=None,
                      epochs_training=100, iterations_test=50, meta_batch_size=32, get_embedding_in_test_phase=False, loss_type="batch_hard_triplet", beta1_triplet=1, beta2_crossEntropy=1):
    omniglot_database = OmniglotDatabase(
        random_seed=47,
        num_train_classes=1200,
        num_val_classes=100,
    )
    if loss_type == "crossEntropy_and_triplet":
        network_cls = SimpleModel
        network_has_class_layer = True
    else:
        network_cls = SimpleModel_noClassLayer
        network_has_class_layer = False

    maml_lossEmbedding = ModelAgnosticMetaLearningModel_lossEmbedding(
        database=omniglot_database,
        network_cls=network_cls,
        n=n_way,
        k=k_shot,
        meta_batch_size=meta_batch_size,
        num_steps_ml=10,
        lr_inner_ml=0.4,
        num_steps_validation=10,
        save_after_epochs=1,
        meta_learning_rate=0.001,
        report_validation_frequency=1,
        log_train_images_after_iteration=-1,
        loss_type=loss_type,
        dataset_name="omniglot",
        beta1_triplet=beta1_triplet, 
        beta2_crossEntropy=beta2_crossEntropy
    )

    if do_train:
        maml_lossEmbedding.train(epochs=epochs_training, epochs_to_load_from=epoch_to_load_for_train)
    else:
        maml_lossEmbedding.evaluate(iterations=iterations_test, epochs_to_load_from=epoch_to_load_for_test, get_embedding=get_embedding_in_test_phase, network_has_class_layer=network_has_class_layer)

def run_mini_imagenet(n_way=5, k_shot=1, do_train=True, epoch_to_load_for_test=1, epoch_to_load_for_train=None, 
                      epochs_training=100, iterations_test=50, meta_batch_size=4, get_embedding_in_test_phase=False, loss_type="batch_hard_triplet", beta1_triplet=1, beta2_crossEntropy=1):
    mini_imagenet_database = MiniImagenetDatabase(random_seed=-1)
    if loss_type == "crossEntropy_and_triplet":
        network_cls = MiniImagenetModel
        network_has_class_layer = True
    else:
        network_cls = MiniImagenetModel_noClassLayer
        network_has_class_layer = False

    maml_lossEmbedding = ModelAgnosticMetaLearningModel_lossEmbedding(
        database=mini_imagenet_database,
        network_cls=network_cls,
        n=n_way,
        k=k_shot,
        meta_batch_size=meta_batch_size,
        num_steps_ml=5,
        lr_inner_ml=0.01,
        num_steps_validation=5,
        save_after_epochs=1,
        meta_learning_rate=0.001,
        report_validation_frequency=1,
        log_train_images_after_iteration=1000,
        least_number_of_tasks_val_test=50,
        clip_gradients=True,
        loss_type=loss_type,
        dataset_name="mini_imagenet",
        beta1_triplet=beta1_triplet, 
        beta2_crossEntropy=beta2_crossEntropy
    )

    if do_train:
        maml_lossEmbedding.train(epochs=epochs_training, epochs_to_load_from=epoch_to_load_for_train)
    else:
        maml_lossEmbedding.evaluate(iterations=iterations_test, epochs_to_load_from=epoch_to_load_for_test, get_embedding=get_embedding_in_test_phase, network_has_class_layer=network_has_class_layer)

def run_CRC(n_way=5, k_shot=1, do_train=True, epoch_to_load_for_test=1, epoch_to_load_for_train=None, 
                      epochs_training=100, iterations_test=50, meta_batch_size=4, get_embedding_in_test_phase=False, loss_type="batch_hard_triplet", beta1_triplet=1, beta2_crossEntropy=1):
    CRC_database = CRCDatabase(random_seed=-1)
    if loss_type == "crossEntropy_and_triplet":
        network_cls = CRCModel
        network_has_class_layer = True
    else:
        network_cls = CRCModel_noClassLayer
        network_has_class_layer = False

    maml_lossEmbedding = ModelAgnosticMetaLearningModel_lossEmbedding(
        database=CRC_database,
        network_cls=network_cls,
        n=n_way,
        k=k_shot,
        meta_batch_size=meta_batch_size,
        num_steps_ml=5,
        lr_inner_ml=0.005,
        num_steps_validation=5,
        save_after_epochs=1,
        meta_learning_rate=0.001,
        report_validation_frequency=1,
        log_train_images_after_iteration=1000,
        least_number_of_tasks_val_test=50,
        clip_gradients=True,
        loss_type=loss_type,
        dataset_name="CRC",
        beta1_triplet=beta1_triplet, 
        beta2_crossEntropy=beta2_crossEntropy
    )

    if do_train:
        maml_lossEmbedding.train(epochs=epochs_training, epochs_to_load_from=epoch_to_load_for_train)
    else:
        maml_lossEmbedding.evaluate(iterations=iterations_test, epochs_to_load_from=epoch_to_load_for_test, get_embedding=get_embedding_in_test_phase, network_has_class_layer=network_has_class_layer)

def run_CRC_cropped(n_way=5, k_shot=1, do_train=True, epoch_to_load_for_test=1, epoch_to_load_for_train=None, 
                      epochs_training=100, iterations_test=50, meta_batch_size=4, get_embedding_in_test_phase=False, loss_type="batch_hard_triplet", beta1_triplet=1, beta2_crossEntropy=1):
    crcDatabase_cropped = CRCDatabase_cropped(random_seed=-1)
    if loss_type == "crossEntropy_and_triplet":
        network_cls = MiniImagenetModel
        network_has_class_layer = True
    else:
        network_cls = MiniImagenetModel_noClassLayer
        network_has_class_layer = False

    maml_lossEmbedding = ModelAgnosticMetaLearningModel_lossEmbedding(
        database=crcDatabase_cropped,
        network_cls=network_cls,
        n=n_way,
        k=k_shot,
        meta_batch_size=meta_batch_size,
        num_steps_ml=5,
        lr_inner_ml=0.01,
        num_steps_validation=5,
        save_after_epochs=1,
        meta_learning_rate=0.001,
        report_validation_frequency=1,
        log_train_images_after_iteration=1000,
        least_number_of_tasks_val_test=50,
        clip_gradients=True,
        loss_type=loss_type,
        dataset_name="CRC_cropped",
        beta1_triplet=beta1_triplet, 
        beta2_crossEntropy=beta2_crossEntropy
    )

    if do_train:
        maml_lossEmbedding.train(epochs=epochs_training, epochs_to_load_from=epoch_to_load_for_train)
    else:
        maml_lossEmbedding.evaluate(iterations=iterations_test, epochs_to_load_from=epoch_to_load_for_test, get_embedding=get_embedding_in_test_phase, network_has_class_layer=network_has_class_layer)


if __name__ == '__main__':
    # settings:
    #---> Note: if you want to debug, make True the flags DEBUGGING and/or DEBUGGING_PLOT_INPUT above pages maml.py and maml_lossEmbedding.py
    do_train = True  #--> True: train, False: test
    dataset = "mini_imagenet"  #--> omniglot, mini_imagenet, CRC, CRC_cropped
    n_way = 5
    k_shot = 1  #--> number of shots in test and validation phases. Note that in UMTRA, training is always 1-shot (2-shot if considering augmentation, too)
    epoch_to_load_for_test = 1
    epoch_to_load_for_train = None  #--> if None, it trains from scratch
    epochs_training = 40  #--> 4000 for mini-imagenet, 4000 for omniglot
    iterations_test = 50  #--> 50 for mini-imagenet, 50 for omniglot
    meta_batch_size = 4  #--> 4 for mini-imagenet, 32 for omniglot
    get_embedding_in_test_phase = True
    loss_type = "crossEntropy_and_triplet"  #--> batch_hard_triplet, crossEntropy_and_triplet
    beta1_triplet, beta2_crossEntropy = 1, 0.1  #--> weights in combined loss function

    if dataset == "omniglot":
        run_omniglot(n_way, k_shot, do_train, epoch_to_load_for_test, epoch_to_load_for_train, epochs_training, iterations_test, meta_batch_size, get_embedding_in_test_phase, loss_type, beta1_triplet, beta2_crossEntropy)
    elif dataset == "mini_imagenet":
        run_mini_imagenet(n_way, k_shot, do_train, epoch_to_load_for_test, epoch_to_load_for_train, epochs_training, iterations_test, meta_batch_size, get_embedding_in_test_phase, loss_type, beta1_triplet, beta2_crossEntropy)
    elif dataset == "CRC":
        run_CRC(n_way, k_shot, do_train, epoch_to_load_for_test, epoch_to_load_for_train, epochs_training, iterations_test, meta_batch_size, get_embedding_in_test_phase, loss_type, beta1_triplet, beta2_crossEntropy)
    elif dataset == "CRC_cropped":
        run_CRC_cropped(n_way, k_shot, do_train, epoch_to_load_for_test, epoch_to_load_for_train, epochs_training, iterations_test, meta_batch_size, get_embedding_in_test_phase, loss_type, beta1_triplet, beta2_crossEntropy)