
### code for cropping the CRC dataset:

import os
import glob
import numpy as np
from PIL import Image

for set_type in ["train", "test", "val"]:
    path = '../datasets/CRC_jpg/' + set_type + "/*"
    class_paths = glob.glob(path)
    for i, class_path in enumerate(class_paths):
        print("processing set {}, class {}/{}".format(set_type, i+1, len(class_paths)))
        class_name = class_path.split("\\")[-1]
        path_to_save = "../datasets/CRC_jpg_cropped/" + set_type + "/" + class_name + "/"
        if not os.path.exists(path_to_save):  
            os.makedirs(path_to_save)
        file_paths = glob.glob(class_path + "/*")
        for file_path in file_paths:
            file_name = file_path.split("\\")[-1]
            file_name = file_name.split(".")[0]
            img = Image.open(file_path)
            img = np.asarray(img)
            img = img[70:-70, 70:-70]
            img = Image.fromarray(img, mode='RGB')
            img.save(path_to_save + file_name + ".jpg")