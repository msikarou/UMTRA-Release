import tensorflow as tf


###################### Loss functions:

def batchHardTriplet_and_crossEntropy_loss(labels, embeddings, logits, margin, squared=False, beta1_triplet=1, beta2_crossEntropy=1):
    loss1 = batch_hard_triplet_loss(labels, embeddings, margin, squared)
    loss2 = tf.reduce_sum(tf.losses.categorical_crossentropy(labels, logits, from_logits=True))
    loss_combined = (beta1_triplet * loss1) + (beta2_crossEntropy * loss2)
    return loss_combined

def batch_hard_triplet_loss(labels, embeddings, margin, squared=False):  #--> Furthest_Nearest_batch_triplet_loss
    # https://github.com/omoindrot/tensorflow-triplet-loss
    # https://omoindrot.github.io/triplet-loss
    # https://github.com/omoindrot/tensorflow-triplet-loss/blob/master/model/triplet_loss.py
    """Build the triplet loss over a batch of embeddings.
    For each anchor, we get the hardest positive and hardest negative to form a triplet.
    Args:
        labels: labels of the batch, of size (batch_size,)
        embeddings: tensor of shape (batch_size, embed_dim)
        margin: margin for triplet loss
        squared: Boolean. If true, output is the pairwise squared euclidean distance matrix.
                    If false, output is the pairwise euclidean distance matrix.
    Returns:
        triplet_loss: scalar tensor containing the triplet loss
    """
    labels = decode_oneHotEncoding_if_is_coded(labels)

    # Get the pairwise distance matrix
    pairwise_dist = pairwise_distances(embeddings, squared=squared)

    # For each anchor, get the hardest positive
    # First, we need to get a mask for every valid positive (they should have same label)
    mask_anchor_positive = get_anchor_positive_triplet_mask(labels)
    mask_anchor_positive = tf.compat.v1.to_float(mask_anchor_positive)

    # We put to 0 any element where (a, p) is not valid (valid if a != p and label(a) == label(p))
    anchor_positive_dist = tf.multiply(mask_anchor_positive, pairwise_dist)

    # shape (batch_size, 1)
    hardest_positive_dist = tf.reduce_max(anchor_positive_dist, axis=1, keepdims=True)

    # For each anchor, get the hardest negative
    # First, we need to get a mask for every valid negative (they should have different labels)
    mask_anchor_negative = get_anchor_negative_triplet_mask(labels)
    mask_anchor_negative = tf.compat.v1.to_float(mask_anchor_negative)

    # We add the maximum value in each row to the invalid negatives (label(a) == label(n))
    max_pairwise_dist_rowwise = tf.reduce_max(pairwise_dist, axis=1, keepdims=True)
    anchor_negative_dist = pairwise_dist + max_pairwise_dist_rowwise * (1.0 - mask_anchor_negative)

    # shape (batch_size,)
    hardest_negative_dist = tf.reduce_min(anchor_negative_dist, axis=1, keepdims=True)

    # Combine biggest d(a, p) and smallest d(a, n) into final triplet loss
    triplet_loss = tf.maximum(hardest_positive_dist - hardest_negative_dist + margin, 0.0)

    # Get final mean triplet loss
    # triplet_loss = tf.reduce_mean(triplet_loss)
    triplet_loss = tf.reduce_sum(triplet_loss)

    return triplet_loss

#### To be completed:
# def loss_triplet(labels, embeddings, margin):
#     if labels.shape[1] > 1:
#         # decode one-hot encoding:
#         labels = tf.argmax(labels, axis=1)
#     labels_unique = tf.unique(labels)[0]
#     n_points = embeddings.shape[0]
#     d_positive = tf.constant(0.0)
#     for class_label in labels_unique:
#         indices_of_points_in_this_class = tf.where(tf.math.equal(labels, tf.constant(class_label)))
#         embeddings_of_points_in_this_class = tf.squeeze(tf.gather(embeddings, indices=indices_of_points_in_this_class, axis=0))
#         d_positive += tf.reduce_sum(self.pairwise_distances(embeddings_of_points_in_this_class, squared=True))
#     # d_pos = tf.reduce_sum(tf.square(self.o1 - self.o2), 1)
#     # d_neg = tf.reduce_sum(tf.square(self.o1 - self.o3), 1)

#     # loss = tf.maximum(0., self.margin_in_loss + d_pos - d_neg)
#     # loss = tf.reduce_mean(loss)
#     return loss

###################### Tools for losses:

def decode_oneHotEncoding_if_is_coded(labels):
    if labels.shape[1] > 1:
        # decode one-hot encoding:
        labels = tf.argmax(labels, axis=1)
    return labels

def pairwise_distances(embeddings, squared=False, l2_normalization=False):
    """Compute the 2D matrix of distances between all the embeddings.
    Args:
        embeddings: tensor of shape (batch_size, embed_dim)
        squared: Boolean. If true, output is the pairwise squared euclidean distance matrix.
                If false, output is the pairwise euclidean distance matrix.
    Returns:
        pairwise_distances: tensor of shape (batch_size, batch_size)
    """
    if l2_normalization:
        embeddings = tf.math.l2_normalize(embeddings)

    # Get the dot product between all embeddings
    # shape (batch_size, batch_size)
    dot_product = tf.matmul(embeddings, tf.transpose(embeddings))

    # Get squared L2 norm for each embedding. We can just take the diagonal of `dot_product`.
    # This also provides more numerical stability (the diagonal of the result will be exactly 0).
    # shape (batch_size,)
    square_norm = tf.linalg.diag_part(dot_product)

    # Compute the pairwise distance matrix as we have:
    # ||a - b||^2 = ||a||^2  - 2 <a, b> + ||b||^2
    # shape (batch_size, batch_size)
    distances = tf.expand_dims(square_norm, 1) - 2.0 * dot_product + tf.expand_dims(square_norm, 0)

    # Because of computation errors, some distances might be negative so we put everything >= 0.0
    distances = tf.maximum(distances, 0.0)

    if not squared:
        # Because the gradient of sqrt is infinite when distances == 0.0 (ex: on the diagonal)
        # we need to add a small epsilon where distances == 0.0
        mask = tf.to_float(tf.equal(distances, 0.0))
        distances = distances + mask * 1e-16

        distances = tf.sqrt(distances)

        # Correct the epsilon added: set the distances on the mask to be exactly 0.0
        distances = distances * (1.0 - mask)

    return distances

def get_anchor_positive_triplet_mask(labels):
    """Return a 2D mask where mask[a, p] is True iff a and p are distinct and have same label.
    Args:
        labels: tf.int32 `Tensor` with shape [batch_size]
    Returns:
        mask: tf.bool `Tensor` with shape [batch_size, batch_size]
    """
    # Check that i and j are distinct
    indices_equal = tf.cast(tf.eye(tf.shape(labels)[0]), tf.bool)
    indices_not_equal = tf.logical_not(indices_equal)

    # Check if labels[i] == labels[j]
    # Uses broadcasting where the 1st argument has shape (1, batch_size) and the 2nd (batch_size, 1)
    labels_equal = tf.equal(tf.expand_dims(labels, 0), tf.expand_dims(labels, 1))

    # Combine the two masks
    mask = tf.logical_and(indices_not_equal, labels_equal)

    return mask


def get_anchor_negative_triplet_mask(labels):
    """Return a 2D mask where mask[a, n] is True iff a and n have distinct labels.
    Args:
        labels: tf.int32 `Tensor` with shape [batch_size]
    Returns:
        mask: tf.bool `Tensor` with shape [batch_size, batch_size]
    """
    # Check if labels[i] != labels[k]
    # Uses broadcasting where the 1st argument has shape (1, batch_size) and the 2nd (batch_size, 1)
    labels_equal = tf.equal(tf.expand_dims(labels, 0), tf.expand_dims(labels, 1))

    mask = tf.logical_not(labels_equal)

    return mask

