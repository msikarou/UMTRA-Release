import os
import pickle
import numpy as np
# import umap
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
from sklearn import preprocessing


def save_variable(variable, name_of_variable, path_to_save='./'):
    # https://stackoverflow.com/questions/6568007/how-do-i-save-and-restore-multiple-variables-in-python
    if not os.path.exists(path_to_save):  # https://stackoverflow.com/questions/273192/how-can-i-create-a-directory-if-it-does-not-exist
        os.makedirs(path_to_save)
    file_address = path_to_save + name_of_variable + '.pckl'
    f = open(file_address, 'wb')
    pickle.dump(variable, f)
    f.close()

def load_variable(name_of_variable, path='./'):
    # https://stackoverflow.com/questions/6568007/how-do-i-save-and-restore-multiple-variables-in-python
    file_address = path + name_of_variable + '.pckl'
    f = open(file_address, 'rb')
    variable = pickle.load(f)
    f.close()
    return variable

def save_np_array_to_txt(variable, name_of_variable, path_to_save='./'):
    if type(variable) is list:
        variable = np.asarray(variable)
    # https://stackoverflow.com/questions/22821460/numpy-save-2d-array-to-text-file/22822701
    if not os.path.exists(path_to_save):  # https://stackoverflow.com/questions/273192/how-can-i-create-a-directory-if-it-does-not-exist
        os.makedirs(path_to_save)
    file_address = path_to_save + name_of_variable + '.txt'
    np.set_printoptions(threshold=np.inf, linewidth=np.inf)  # turn off summarization, line-wrapping
    with open(file_address, 'w') as f:
        f.write(np.array2string(variable, separator=', '))

def evaluate_embedding(embedding, labels, k_list=[1, 2, 4, 8, 16]):
    # https://github.com/chaoyuaw/incubator-mxnet/blob/master/example/gluon/embedding_learning/train.py
    """Evaluate embeddings based on Recall@k."""
    d_mat = get_distance_matrix(embedding)
    # d_mat = d_mat.asnumpy()
    # labels = labels.asnumpy()
    le = preprocessing.LabelEncoder()
    le.fit(np.unique(np.asarray(labels)))
    labels = le.transform(labels)
    recall_at = []
    for k in k_list:
        # print('Recall@%d' % k)
        correct, cnt = 0.0, 0.0
        for i in range(embedding.shape[0]):
            d_mat[i, i] = np.inf
            # https://stackoverflow.com/questions/42184499/cannot-understand-numpy-argpartition-output
            nns = np.argpartition(d_mat[i], k)[:k]   
            if any(labels[i] == labels[nn] for nn in nns):
                correct += 1
            cnt += 1
        recall_at.append(correct/cnt)
    k_list = np.asarray(k_list)
    recall_at = np.asarray(recall_at)
    return k_list, recall_at

def get_distance_matrix(x):
    # https://github.com/chaoyuaw/incubator-mxnet/blob/master/example/gluon/embedding_learning/train.py
    """Get distance matrix given a matrix. Used in testing."""
    square = np.sum(x ** 2.0, axis=1).reshape((-1, 1))
    distance_square = square + square.transpose() - (2.0 * np.dot(x, x.transpose()))
    distance_square[distance_square < 0] = 0
    return np.sqrt(distance_square)

def plot_embedding_of_points(embedding, labels, n_samples_plot=None, path_to_save=None, translation_of_class_names=None, name_of_plot="embedding"):
    labels = np.asarray(labels)
    n_samples = embedding.shape[0]
    if n_samples_plot != None:
        indices_to_plot = np.random.choice(range(n_samples), min(n_samples_plot, n_samples), replace=False)
    else:
        indices_to_plot = np.random.choice(range(n_samples), n_samples, replace=False)
    indices_to_plot = list(indices_to_plot)
    embedding_sampled = embedding[indices_to_plot, :]
    if embedding.shape[1] == 2:
        pass
    else:
        # embedding_sampled = umap.UMAP(n_neighbors=500).fit_transform(embedding_sampled)
        embedding_sampled = TSNE(n_components=2).fit_transform(embedding_sampled)
    n_points = embedding.shape[0]
    # n_points_sampled = embedding_sampled.shape[0]
    _, ax = plt.subplots(1, figsize=(14, 10))
    if translation_of_class_names is None:
        if type(labels[0]) == str:
            labels = list(map(int, labels))  #--> convert from strings to integers
        labels_sampled = labels[indices_to_plot]
        classes = np.unique(labels_sampled)
    elif translation_of_class_names == "mini_imagenet":
        unique_labels = list(np.unique(labels))
        labels = np.asarray([unique_labels.index(label) for label in labels]) #--> make labels numeric (integers)
        labels_sampled = labels[indices_to_plot]
        classes = get_class_names_mini_imagenet(unique_labels)
    elif translation_of_class_names == "omniglot":
        pass
    elif translation_of_class_names == "CRC" or translation_of_class_names == "CRC_cropped":
        unique_labels = list(np.unique(labels))
        labels = np.asarray([unique_labels.index(label) for label in labels]) #--> make labels numeric (integers)
        labels_sampled = labels[indices_to_plot]
        classes = get_class_names_CRC(unique_labels)
    n_classes = len(classes)
    plt.scatter(embedding_sampled[:, 0], embedding_sampled[:, 1], s=10, c=labels_sampled, cmap='Spectral', alpha=1.0)
    # plt.setp(ax, xticks=[], yticks=[])
    cbar = plt.colorbar(boundaries=np.arange(n_classes+1)-0.5)
    # cbar = plt.colorbar(boundaries=np.arange(n_classes+1)-0.7)
    cbar.set_ticks(np.arange(n_classes))
    cbar.set_ticklabels(classes)
    if path_to_save is not None:
        if not os.path.exists(path_to_save):  # https://stackoverflow.com/questions/273192/how-can-i-create-a-directory-if-it-does-not-exist
            os.makedirs(path_to_save)
        plt.savefig(path_to_save+name_of_plot+".png")
        plt.clf()
        plt.close()
    return plt, indices_to_plot

def get_class_names_mini_imagenet(unique_labels):
    # https://gist.github.com/kaixin96/ffb88bd025fc05deb2d7f1378e9b7282
    classes = []
    for str_label in unique_labels:
        if str_label == "n01930112":
            classes.append("nematode")
        elif str_label == "n01981276":
            classes.append("king crab")
        elif str_label == "n02099601":
            classes.append("golden retriever")
        elif str_label == "n02110063":
            classes.append("malamute")
        elif str_label == "n02110341":
            classes.append("dalmatian")
        elif str_label == "n02116738":
            classes.append("African hunting dog")
        elif str_label == "n02129165":
            classes.append("lion")
        elif str_label == "n02219486":
            classes.append("ant")
        elif str_label == "n02443484":
            classes.append("black-footed ferret")
        elif str_label == "n02871525":
            classes.append("bookshop")
        elif str_label == "n03127925":
            classes.append("crate")
        elif str_label == "n03146219":
            classes.append("cuirass")
        elif str_label == "n03272010":
            classes.append("electric guitar")
        elif str_label == "n03544143":
            classes.append("hourglass")
        elif str_label == "n03775546":
            classes.append("mixing bowl")
        elif str_label == "n04146614":
            classes.append("school bus")
        elif str_label == "n04149813":
            classes.append("scoreboard")
        elif str_label == "n04418357":
            classes.append("theater curtain")
        elif str_label == "n04522168":
            classes.append("vase")
        elif str_label == "n07613480":
            classes.append("trifle")
    return classes

def get_class_names_CRC(unique_labels):
    classes = []
    for str_label in unique_labels:
        if str_label == "00_TUMOR":
            classes.append("Tumor")
        elif str_label == "01_STROMA":
            classes.append("Stroma")
        elif str_label == "02_MUCUS":
            classes.append("Mucus")
        elif str_label == "03_LYMPHO":
            classes.append("Lympho")
        elif str_label == "04_DEBRIS":
            classes.append("Debris")
        elif str_label == "05_SMOOTH_MUSCLE":
            classes.append("Smooth muscle")
        elif str_label == "06_ADIPOSE":
            classes.append("Adipose")
        elif str_label == "07_BACKGROUND":
            classes.append("Background")
        elif str_label == "08_NORMAL":
            classes.append("Normal")
    return classes

def append_to_text_file(path_to_save, name_of_file, line_to_add):
    if not os.path.exists(path_to_save):  
        os.makedirs(path_to_save)
    try:
        with open(path_to_save+name_of_file+".txt", "a") as myfile:
            myfile.write(line_to_add)
    except:  #--> if file does not exist
        with open(path_to_save+name_of_file+".txt", "w+") as myfile:
            myfile.write(line_to_add)