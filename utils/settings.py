import os

PROJECT_ROOT_ADDRESS = os.path.expanduser('~/programming/meta-learning-framework')
# OMNIGLOT_RAW_DATA_ADDRESS = os.path.expanduser('~/datasets/omniglot')
# OMNIGLOT_RAW_DATA_ADDRESS = os.path.expanduser('../archive/omniglot')
IMAGENET_RAW_DATA_ADDRESS = os.path.expanduser('~/datasets/imagenet/')
# MINI_IMAGENET_RAW_DATA_ADDRESS = os.path.expanduser('~/datasets/mini-imagenet/')
MINI_IMAGENET_RAW_DATA_ADDRESS = os.path.expanduser('../datasets/mini_imagenet/')
CRC_RAW_DATA_ADDRESS = os.path.expanduser('../datasets/CRC_jpg/')
CRC_CROPPED_RAW_DATA_ADDRESS = os.path.expanduser('../datasets/CRC_jpg_cropped/')
